﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoClickProgram
{
    public partial class fMain : Form
    {
        [DllImport("user32.dll")]
        static extern void SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        static extern void keybd_event(byte vk, byte scan, int flags, ref int extrainfo);
        
        [DllImport("user32.dll")]
        static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint dwData, int dwExtraInfo);
        private const uint LBUTTONDOWN = 0x0002;
        private const uint LBUTTONUP = 0x0004;
        
        [DllImport("user32.dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(int hWnd);
        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(int hWnd, int nCmdShow);
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;

        private bool m_bStart = false;
        private bool m_bSpace = false;

        private System.Timers.Timer m_ClickTimer;
        private System.Timers.Timer m_PosTimer;

        private int cnt;
        private int countSet;
        private bool countUse;
        private int[,] posSet;
        private uint[] timerSet;
        private bool[] useSet;

        static bool On = false;
        static Point Pos = new Point();

        public fMain()
        {
            InitializeComponent();
            panel1.MouseDown += (o, e) => { if (e.Button == MouseButtons.Left) { On = true; Pos = e.Location; } };
            panel1.MouseMove += (o, e) => { if (On) Location = new Point(Location.X + (e.X - Pos.X), Location.Y + (e.Y - Pos.Y)); };
            panel1.MouseUp += (o, e) => { if (e.Button == MouseButtons.Left) { On = false; Pos = e.Location; } };
            panel4.MouseDown += (o, e) => { if (e.Button == MouseButtons.Left) { On = true; Pos = e.Location; } };
            panel4.MouseMove += (o, e) => { if (On) Location = new Point(Location.X + (e.X - Pos.X), Location.Y + (e.Y - Pos.Y)); };
            panel4.MouseUp += (o, e) => { if (e.Button == MouseButtons.Left) { On = false; Pos = e.Location; } };
            btnMinimize.Click += (o, e) => { WindowState = FormWindowState.Minimized; };

            KeyPreview = true;
            m_ClickTimer = new System.Timers.Timer();
            m_ClickTimer.Elapsed += new System.Timers.ElapsedEventHandler(ClickTimeEvent);
            m_PosTimer = new System.Timers.Timer();
            m_PosTimer.Elapsed += new System.Timers.ElapsedEventHandler(PositionEvent);
            m_PosTimer.Interval = 100;

            cnt = 0;
            countSet = 0;
            countUse = false;
            posSet = new int[5, 2];
            timerSet = new uint[5];
            useSet = new bool[5];

            lblToolTip.Text = Str.ready;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            m_bStart = !m_bStart;
            
            if (!m_bStart)
            {
                StartStopUIChange(m_bStart);
                m_ClickTimer.Enabled = m_bStart;
                lblToolTip.Text = Str.stop;
            }
            else
            {
                if (countUse) { 
                    cnt = 0;
                    try {
                        countSet = Convert.ToInt32(txtCount.Text);
                    } catch (Exception) {
                        countSet = 1;
                    }
                }

                try {
                    timerSet[0] = Convert.ToUInt32(txtTimer1.Text.ToString());
                    timerSet[1] = Convert.ToUInt32(txtTimer2.Text.ToString());
                    timerSet[2] = Convert.ToUInt32(txtTimer3.Text.ToString());
                    timerSet[3] = Convert.ToUInt32(txtTimer4.Text.ToString());
                    timerSet[4] = Convert.ToUInt32(txtTimer5.Text.ToString());
                } catch { }

                if (useSet[0] && timerSet[0] > 0) {
                    if(useSet[1] && timerSet[1] > 0) {
                        if (useSet[2] && timerSet[2] > 0) {
                            if (useSet[3] && timerSet[3] > 0) {
                                if (useSet[4] && timerSet[4] > 0) {
                                    m_ClickTimer.Interval = timerSet[0] + timerSet[1] + timerSet[2] + timerSet[3] + timerSet[4];
                                } else {
                                    m_ClickTimer.Interval = timerSet[0] + timerSet[1] + timerSet[2] + timerSet[3];
                                }
                            } else {
                                m_ClickTimer.Interval = timerSet[0] + timerSet[1] + timerSet[2];
                            }
                        } else {
                            m_ClickTimer.Interval = timerSet[0] + timerSet[1];
                        }
                    } else {
                        m_ClickTimer.Interval = timerSet[0];
                    }

                    StartStopUIChange(m_bStart);
                    m_ClickTimer.Enabled = m_bStart;
                    lblToolTip.Text = Str.start;
                } 
                else 
                {
                    m_bStart = !m_bStart;
                    StartStopUIChange(m_bStart);
                    m_ClickTimer.Enabled = m_bStart;
                    lblToolTip.Text = Str.strStartAlarm;
                }
            }
        }

        private void StartStopUIChange(bool bCheck)
        {
            if(bCheck)
            {
                btnStart.Text = Str.stop;
                btnStart.BackColor = Color.DimGray;
            } else
            {
                btnStart.Text = Str.start;
                btnStart.BackColor = Color.RoyalBlue;
            }
            txtTimer1.ReadOnly = bCheck;
            txtTimer2.ReadOnly = bCheck;
            txtTimer3.ReadOnly = bCheck;
            txtTimer4.ReadOnly = bCheck;
            txtTimer5.ReadOnly = bCheck;
            cbCount.Enabled = !bCheck;
            cbPos1.Enabled = !bCheck;
            cbPos2.Enabled = !bCheck;
            cbPos3.Enabled = !bCheck;
            cbPos4.Enabled = !bCheck;
            cbPos5.Enabled = !bCheck;
            btnPosSet1.Enabled = !bCheck;
            btnPosSet2.Enabled = !bCheck;
            btnPosSet3.Enabled = !bCheck;
            btnPosSet4.Enabled = !bCheck;
            btnPosSet5.Enabled = !bCheck;
        }

        private void ClickTimeEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(countUse)
            {
                if (cnt == countSet)
                {
                    Invoke(new MethodInvoker(delegate
                    {
                        cnt = 0;
                        m_bStart = !m_bStart;
                        btnStart.Text = Str.start;
                        btnStart.BackColor = Color.RoyalBlue;
                        txtTimer1.ReadOnly = false;
                        txtTimer2.ReadOnly = false;
                        txtTimer3.ReadOnly = false;
                        txtTimer4.ReadOnly = false;
                        txtTimer5.ReadOnly = false;
                        m_ClickTimer.Enabled = m_bStart;
                        lblToolTip.Text = Str.strStartAlarm;
                    }));
                }
                else
                {
                    cnt++;
                    if (useSet[0]) mouseClick(new Point(posSet[0, 0], posSet[0, 1]));
                    if (useSet[1])
                    {
                        Wait(Convert.ToInt32(timerSet[0]));
                        mouseClick(new Point(posSet[1, 0], posSet[1, 1]));
                    }
                    if (useSet[2])
                    {
                        Wait(Convert.ToInt32(timerSet[1]));
                        mouseClick(new Point(posSet[2, 0], posSet[2, 1]));
                    }
                    if (useSet[3])
                    {
                        Wait(Convert.ToInt32(timerSet[2]));
                        mouseClick(new Point(posSet[3, 0], posSet[3, 1]));
                    }
                    if (useSet[4])
                    {
                        Wait(Convert.ToInt32(timerSet[3]));
                        mouseClick(new Point(posSet[4, 0], posSet[4, 1]));
                    }
                }
            } else
            {
                if (useSet[0]) mouseClick(new Point(posSet[0, 0], posSet[0, 1]));
                if (useSet[1])
                {
                    Wait(Convert.ToInt32(timerSet[0]));
                    mouseClick(new Point(posSet[1, 0], posSet[1, 1]));
                }
                if (useSet[2])
                {
                    Wait(Convert.ToInt32(timerSet[1]));
                    mouseClick(new Point(posSet[2, 0], posSet[2, 1]));
                }
                if (useSet[3])
                {
                    Wait(Convert.ToInt32(timerSet[2]));
                    mouseClick(new Point(posSet[3, 0], posSet[3, 1]));
                }
                if (useSet[4])
                {
                    Wait(Convert.ToInt32(timerSet[3]));
                    mouseClick(new Point(posSet[4, 0], posSet[4, 1]));
                }
            }
            
        }

        private void mouseClick(Point pos)
        {
            SetCursorPos(pos.X, pos.Y);
            mouse_event(LBUTTONDOWN, 0, 0, 0, 0);
            mouse_event(LBUTTONUP, 0, 0, 0, 0);
            int hWnd = FindWindow(null, "Auto Click Program v.3.23");
            if(!hWnd.Equals(0))
            {
                ShowWindowAsync(hWnd, SW_SHOWNORMAL);
                SetForegroundWindow(hWnd);
            }
        }

        private void PositionEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                lblMPos.Text = $"( {MousePosition.X} , {MousePosition.Y}) ";
            }));
        }

        public static void Wait(int milliseconds = 1)
        {
            int Timing = Environment.TickCount + milliseconds;
            while (Timing > Environment.TickCount)
            {
                Application.DoEvents();
                Thread.Sleep(1);
            }
        }

        private void cbPos1_CheckedChanged(object sender, EventArgs e)
        {
            useSet[0] = cbPos1.Checked;
        }
        private void cbPos2_CheckedChanged(object sender, EventArgs e)
        {
            useSet[1] = cbPos2.Checked;
        }
        private void cbPos3_CheckedChanged(object sender, EventArgs e)
        {
            useSet[2] = cbPos3.Checked;
        }
        private void cbPos4_CheckedChanged(object sender, EventArgs e)
        {
            useSet[3] = cbPos4.Checked;
        }
        private void cbPos5_CheckedChanged(object sender, EventArgs e)
        {
            useSet[4] = cbPos5.Checked;
        }

        private void btnPosSet1_Click(object sender, EventArgs e)
        {
            lblMPos.Location = lblPos1.Location;
            m_bSpace = !m_bSpace;
            m_PosTimer.Enabled = m_bSpace;
        }
        private void btnPosSet2_Click(object sender, EventArgs e)
        {
            lblMPos.Location = lblPos2.Location;
            m_bSpace = !m_bSpace;
            m_PosTimer.Enabled = m_bSpace;
        }
        private void btnPosSet3_Click(object sender, EventArgs e)
        {
            lblMPos.Location = lblPos3.Location;
            m_bSpace = !m_bSpace;
            m_PosTimer.Enabled = m_bSpace;
        }
        private void btnPosSet4_Click(object sender, EventArgs e)
        {
            lblMPos.Location = lblPos4.Location;
            m_bSpace = !m_bSpace;
            m_PosTimer.Enabled = m_bSpace;
        }
        private void btnPosSet5_Click(object sender, EventArgs e)
        {
            lblMPos.Location = lblPos5.Location;
            m_bSpace = !m_bSpace;
            m_PosTimer.Enabled = m_bSpace;
        }

        private void btnPosSet1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                posSet[0, 0] = MousePosition.X;
                posSet[0, 1] = MousePosition.Y;
                lblPos1.Text = $"( {posSet[0, 0]} , {posSet[0, 1]}) ";
                m_bSpace = true;
            }
        }
        private void btnPosSet2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                posSet[1, 0] = MousePosition.X;
                posSet[1, 1] = MousePosition.Y;
                lblPos2.Text = $"( {posSet[1, 0]} , {posSet[1, 1]}) ";
                m_bSpace = true;
            }
        }
        private void btnPosSet3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                posSet[2, 0] = MousePosition.X;
                posSet[2, 1] = MousePosition.Y;
                lblPos3.Text = $"( {posSet[2, 0]} , {posSet[2, 1]}) ";
                m_bSpace = true;
            }
        }
        private void btnPosSet4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                posSet[3, 0] = MousePosition.X;
                posSet[3, 1] = MousePosition.Y;
                lblPos4.Text = $"( {posSet[3, 0]} , {posSet[3, 1]}) ";
                m_bSpace = true;
            }
        }
        private void btnPosSet5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                posSet[4, 0] = MousePosition.X;
                posSet[4, 1] = MousePosition.Y;
                lblPos5.Text = $"( {posSet[4, 0]} , {posSet[4, 1]}) ";
                m_bSpace = true;
            }
        }

        private void txtTimer1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyCode = (int)e.KeyChar;
            if ((KeyCode < 48 || KeyCode > 57) && KeyCode != 8 && KeyCode != 46)
            {
                e.Handled = true;
            }
        }
        private void txtTimer2_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyCode = (int)e.KeyChar;
            if ((KeyCode < 48 || KeyCode > 57) && KeyCode != 8 && KeyCode != 46)
            {
                e.Handled = true;
            }
        }
        private void txtTimer3_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyCode = (int)e.KeyChar;
            if ((KeyCode < 48 || KeyCode > 57) && KeyCode != 8 && KeyCode != 46)
            {
                e.Handled = true;
            }
        }
        private void txtTimer4_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyCode = (int)e.KeyChar;
            if ((KeyCode < 48 || KeyCode > 57) && KeyCode != 8 && KeyCode != 46)
            {
                e.Handled = true;
            }
        }
        private void txtTimer5_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyCode = (int)e.KeyChar;
            if ((KeyCode < 48 || KeyCode > 57) && KeyCode != 8 && KeyCode != 46)
            {
                e.Handled = true;
            }
        }

        private void btnPosSet1_MouseHover(object sender, EventArgs e)
        {
            lblToolTip.Text = Str.strPosSet;
        }
        private void txtTimer1_MouseHover(object sender, EventArgs e)
        {
            lblToolTip.Text = Str.strTimerSet;
        }
        private void cbPos1_MouseHover(object sender, EventArgs e)
        {
            lblToolTip.Text = Str.strPosChk;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex == 0)
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                SetTextLanguage();
            } else
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("ko-KR");
                SetTextLanguage();
            }
        }

        private void SetTextLanguage()
        {
            btnStart.Text = Str.start;
            lblUse.Text = Str.use;
            lblPosition.Text = Str.position;
            lblPositionSet.Text = Str.position + " " + Str.set;
            lblTimer.Text = Str.timer;
            cbPos1.Text = Str.position + "1";
            cbPos2.Text = Str.position + "2";
            cbPos3.Text = Str.position + "3";
            cbPos4.Text = Str.position + "4";
            cbPos5.Text = Str.position + "5";
            btnPosSet1.Text = Str.set;
            btnPosSet2.Text = Str.set;
            btnPosSet3.Text = Str.set;
            btnPosSet4.Text = Str.set;
            btnPosSet5.Text = Str.set;
        }

        private void cbCount_CheckedChanged(object sender, EventArgs e)
        {
            countUse = cbCount.Checked;
        }
        private void txtCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyCode = (int)e.KeyChar;
            if ((KeyCode < 48 || KeyCode > 57) && KeyCode != 8 && KeyCode != 46)
            {
                e.Handled = true;
            }
        }

        private void txtCount_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                try
                {
                    countSet = Convert.ToInt32(txtCount.Text.ToString());
                    txtCount.ForeColor = Color.White;
                    txtCount.BackColor = Color.DimGray;
                }
                catch (Exception)
                {
                    lblToolTip.Text = Str.strTimerAlarm;
                }
            }
        }

        private void txtTimer1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    timerSet[0] = Convert.ToUInt32(txtTimer1.Text.ToString());
                    txtTimer1.ForeColor = Color.White;
                    txtTimer1.BackColor = Color.DimGray;
                    txtTimer2.Focus();
                }
                catch (Exception)
                {
                    lblToolTip.Text = Str.strTimerAlarm;
                }
            }
        }
        private void txtTimer2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    timerSet[1] = Convert.ToUInt32(txtTimer2.Text.ToString());
                    txtTimer2.ForeColor = Color.White;
                    txtTimer2.BackColor = Color.DimGray;
                    txtTimer3.Focus();
                }
                catch (Exception)
                {
                    lblToolTip.Text = Str.strTimerAlarm;
                }
            }
        }
        private void txtTimer3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    timerSet[2] = Convert.ToUInt32(txtTimer3.Text.ToString());
                    txtTimer3.ForeColor = Color.White;
                    txtTimer3.BackColor = Color.DimGray;
                    txtTimer4.Focus();
                }
                catch (Exception)
                {
                    lblToolTip.Text = Str.strTimerAlarm;
                }
            }
        }
        private void txtTimer4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    timerSet[3] = Convert.ToUInt32(txtTimer4.Text.ToString());
                    txtTimer4.ForeColor = Color.White;
                    txtTimer4.BackColor = Color.DimGray;
                    txtTimer5.Focus();
                }
                catch (Exception)
                {
                    lblToolTip.Text = Str.strTimerAlarm;
                }
            }
        }
        private void txtTimer5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    timerSet[4] = Convert.ToUInt32(txtTimer5.Text.ToString());
                    txtTimer5.ForeColor = Color.White;
                    txtTimer5.BackColor = Color.DimGray;
                }
                catch (Exception)
                {
                    lblToolTip.Text = Str.strTimerAlarm;
                }
            }
        }

        private void txtCount_TextChanged(object sender, EventArgs e)
        {
            txtCount.ForeColor = Color.Black;
            txtCount.BackColor = Color.DarkGray;
        }
        private void txtTimer1_TextChanged(object sender, EventArgs e)
        {
            txtTimer1.ForeColor = Color.Black;
            txtTimer1.BackColor = Color.DarkGray;
        }
        private void txtTimer2_TextChanged(object sender, EventArgs e)
        {
            txtTimer2.ForeColor = Color.Black;
            txtTimer2.BackColor = Color.DarkGray;
        }
        private void txtTimer3_TextChanged(object sender, EventArgs e)
        {
            txtTimer3.ForeColor = Color.Black;
            txtTimer3.BackColor = Color.DarkGray;
        }
        private void txtTimer4_TextChanged(object sender, EventArgs e)
        {
            txtTimer4.ForeColor = Color.Black;
            txtTimer4.BackColor = Color.DarkGray;
        }
        private void txtTimer5_TextChanged(object sender, EventArgs e)
        {
            txtTimer5.ForeColor = Color.Black;
            txtTimer5.BackColor = Color.DarkGray;
        }

        private void fMain_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Modifiers == Keys.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.D2: //Ctrl + 2
                        if (m_bStart)
                        {
                            m_bStart = !m_bStart;
                            StartStopUIChange(m_bStart);
                            m_ClickTimer.Enabled = m_bStart;
                            lblToolTip.Text = Str.stop;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void btnStart_MouseHover(object sender, EventArgs e)
        {
            lblToolTip.Text = Str.strCtrl2;
        }
    }
}
