﻿namespace AutoClickProgram
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.cbPos1 = new System.Windows.Forms.CheckBox();
            this.cbPos2 = new System.Windows.Forms.CheckBox();
            this.cbPos3 = new System.Windows.Forms.CheckBox();
            this.cbPos4 = new System.Windows.Forms.CheckBox();
            this.cbPos5 = new System.Windows.Forms.CheckBox();
            this.btnPosSet1 = new System.Windows.Forms.Button();
            this.btnPosSet2 = new System.Windows.Forms.Button();
            this.btnPosSet3 = new System.Windows.Forms.Button();
            this.btnPosSet4 = new System.Windows.Forms.Button();
            this.btnPosSet5 = new System.Windows.Forms.Button();
            this.lblPos1 = new System.Windows.Forms.Label();
            this.lblPos2 = new System.Windows.Forms.Label();
            this.lblPos3 = new System.Windows.Forms.Label();
            this.lblPos4 = new System.Windows.Forms.Label();
            this.lblPos5 = new System.Windows.Forms.Label();
            this.txtTimer1 = new System.Windows.Forms.TextBox();
            this.txtTimer2 = new System.Windows.Forms.TextBox();
            this.txtTimer3 = new System.Windows.Forms.TextBox();
            this.txtTimer4 = new System.Windows.Forms.TextBox();
            this.txtTimer5 = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblUse = new System.Windows.Forms.Label();
            this.lblPosition = new System.Windows.Forms.Label();
            this.lblPositionSet = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblMPos = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbCount = new System.Windows.Forms.CheckBox();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.lblToolTip = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbPos1
            // 
            this.cbPos1.AutoSize = true;
            this.cbPos1.Location = new System.Drawing.Point(18, 126);
            this.cbPos1.Name = "cbPos1";
            this.cbPos1.Size = new System.Drawing.Size(83, 19);
            this.cbPos1.TabIndex = 0;
            this.cbPos1.Text = "Position1";
            this.cbPos1.UseVisualStyleBackColor = true;
            this.cbPos1.CheckedChanged += new System.EventHandler(this.cbPos1_CheckedChanged);
            this.cbPos1.MouseHover += new System.EventHandler(this.cbPos1_MouseHover);
            // 
            // cbPos2
            // 
            this.cbPos2.AutoSize = true;
            this.cbPos2.Location = new System.Drawing.Point(18, 153);
            this.cbPos2.Name = "cbPos2";
            this.cbPos2.Size = new System.Drawing.Size(83, 19);
            this.cbPos2.TabIndex = 0;
            this.cbPos2.Text = "Position2";
            this.cbPos2.UseVisualStyleBackColor = true;
            this.cbPos2.CheckedChanged += new System.EventHandler(this.cbPos2_CheckedChanged);
            this.cbPos2.MouseHover += new System.EventHandler(this.cbPos1_MouseHover);
            // 
            // cbPos3
            // 
            this.cbPos3.AutoSize = true;
            this.cbPos3.Location = new System.Drawing.Point(18, 180);
            this.cbPos3.Name = "cbPos3";
            this.cbPos3.Size = new System.Drawing.Size(83, 19);
            this.cbPos3.TabIndex = 0;
            this.cbPos3.Text = "Position3";
            this.cbPos3.UseVisualStyleBackColor = true;
            this.cbPos3.CheckedChanged += new System.EventHandler(this.cbPos3_CheckedChanged);
            this.cbPos3.MouseHover += new System.EventHandler(this.cbPos1_MouseHover);
            // 
            // cbPos4
            // 
            this.cbPos4.AutoSize = true;
            this.cbPos4.Location = new System.Drawing.Point(18, 207);
            this.cbPos4.Name = "cbPos4";
            this.cbPos4.Size = new System.Drawing.Size(83, 19);
            this.cbPos4.TabIndex = 0;
            this.cbPos4.Text = "Position4";
            this.cbPos4.UseVisualStyleBackColor = true;
            this.cbPos4.CheckedChanged += new System.EventHandler(this.cbPos4_CheckedChanged);
            this.cbPos4.MouseHover += new System.EventHandler(this.cbPos1_MouseHover);
            // 
            // cbPos5
            // 
            this.cbPos5.AutoSize = true;
            this.cbPos5.Location = new System.Drawing.Point(18, 234);
            this.cbPos5.Name = "cbPos5";
            this.cbPos5.Size = new System.Drawing.Size(83, 19);
            this.cbPos5.TabIndex = 0;
            this.cbPos5.Text = "Position5";
            this.cbPos5.UseVisualStyleBackColor = true;
            this.cbPos5.CheckedChanged += new System.EventHandler(this.cbPos5_CheckedChanged);
            this.cbPos5.MouseHover += new System.EventHandler(this.cbPos1_MouseHover);
            // 
            // btnPosSet1
            // 
            this.btnPosSet1.BackColor = System.Drawing.Color.DimGray;
            this.btnPosSet1.FlatAppearance.BorderSize = 0;
            this.btnPosSet1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosSet1.Location = new System.Drawing.Point(216, 125);
            this.btnPosSet1.Name = "btnPosSet1";
            this.btnPosSet1.Size = new System.Drawing.Size(75, 22);
            this.btnPosSet1.TabIndex = 1;
            this.btnPosSet1.Text = "SET";
            this.btnPosSet1.UseVisualStyleBackColor = false;
            this.btnPosSet1.Click += new System.EventHandler(this.btnPosSet1_Click);
            this.btnPosSet1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPosSet1_KeyDown);
            this.btnPosSet1.MouseHover += new System.EventHandler(this.btnPosSet1_MouseHover);
            // 
            // btnPosSet2
            // 
            this.btnPosSet2.BackColor = System.Drawing.Color.DimGray;
            this.btnPosSet2.FlatAppearance.BorderSize = 0;
            this.btnPosSet2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosSet2.Location = new System.Drawing.Point(216, 151);
            this.btnPosSet2.Name = "btnPosSet2";
            this.btnPosSet2.Size = new System.Drawing.Size(75, 22);
            this.btnPosSet2.TabIndex = 1;
            this.btnPosSet2.Text = "SET";
            this.btnPosSet2.UseVisualStyleBackColor = false;
            this.btnPosSet2.Click += new System.EventHandler(this.btnPosSet2_Click);
            this.btnPosSet2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPosSet2_KeyDown);
            this.btnPosSet2.MouseHover += new System.EventHandler(this.btnPosSet1_MouseHover);
            // 
            // btnPosSet3
            // 
            this.btnPosSet3.BackColor = System.Drawing.Color.DimGray;
            this.btnPosSet3.FlatAppearance.BorderSize = 0;
            this.btnPosSet3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosSet3.Location = new System.Drawing.Point(216, 179);
            this.btnPosSet3.Name = "btnPosSet3";
            this.btnPosSet3.Size = new System.Drawing.Size(75, 22);
            this.btnPosSet3.TabIndex = 1;
            this.btnPosSet3.Text = "SET";
            this.btnPosSet3.UseVisualStyleBackColor = false;
            this.btnPosSet3.Click += new System.EventHandler(this.btnPosSet3_Click);
            this.btnPosSet3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPosSet3_KeyDown);
            this.btnPosSet3.MouseHover += new System.EventHandler(this.btnPosSet1_MouseHover);
            // 
            // btnPosSet4
            // 
            this.btnPosSet4.BackColor = System.Drawing.Color.DimGray;
            this.btnPosSet4.FlatAppearance.BorderSize = 0;
            this.btnPosSet4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosSet4.Location = new System.Drawing.Point(216, 206);
            this.btnPosSet4.Name = "btnPosSet4";
            this.btnPosSet4.Size = new System.Drawing.Size(75, 22);
            this.btnPosSet4.TabIndex = 1;
            this.btnPosSet4.Text = "SET";
            this.btnPosSet4.UseVisualStyleBackColor = false;
            this.btnPosSet4.Click += new System.EventHandler(this.btnPosSet4_Click);
            this.btnPosSet4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPosSet4_KeyDown);
            this.btnPosSet4.MouseHover += new System.EventHandler(this.btnPosSet1_MouseHover);
            // 
            // btnPosSet5
            // 
            this.btnPosSet5.BackColor = System.Drawing.Color.DimGray;
            this.btnPosSet5.FlatAppearance.BorderSize = 0;
            this.btnPosSet5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosSet5.Location = new System.Drawing.Point(216, 233);
            this.btnPosSet5.Name = "btnPosSet5";
            this.btnPosSet5.Size = new System.Drawing.Size(75, 22);
            this.btnPosSet5.TabIndex = 1;
            this.btnPosSet5.Text = "SET";
            this.btnPosSet5.UseVisualStyleBackColor = false;
            this.btnPosSet5.Click += new System.EventHandler(this.btnPosSet5_Click);
            this.btnPosSet5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPosSet5_KeyDown);
            this.btnPosSet5.MouseHover += new System.EventHandler(this.btnPosSet1_MouseHover);
            // 
            // lblPos1
            // 
            this.lblPos1.AutoSize = true;
            this.lblPos1.Location = new System.Drawing.Point(105, 129);
            this.lblPos1.Name = "lblPos1";
            this.lblPos1.Size = new System.Drawing.Size(53, 15);
            this.lblPos1.TabIndex = 2;
            this.lblPos1.Text = "( 0 , 0 )";
            // 
            // lblPos2
            // 
            this.lblPos2.AutoSize = true;
            this.lblPos2.Location = new System.Drawing.Point(105, 156);
            this.lblPos2.Name = "lblPos2";
            this.lblPos2.Size = new System.Drawing.Size(53, 15);
            this.lblPos2.TabIndex = 2;
            this.lblPos2.Text = "( 0 , 0 )";
            // 
            // lblPos3
            // 
            this.lblPos3.AutoSize = true;
            this.lblPos3.Location = new System.Drawing.Point(105, 183);
            this.lblPos3.Name = "lblPos3";
            this.lblPos3.Size = new System.Drawing.Size(53, 15);
            this.lblPos3.TabIndex = 2;
            this.lblPos3.Text = "( 0 , 0 )";
            // 
            // lblPos4
            // 
            this.lblPos4.AutoSize = true;
            this.lblPos4.Location = new System.Drawing.Point(105, 210);
            this.lblPos4.Name = "lblPos4";
            this.lblPos4.Size = new System.Drawing.Size(53, 15);
            this.lblPos4.TabIndex = 2;
            this.lblPos4.Text = "( 0 , 0 )";
            // 
            // lblPos5
            // 
            this.lblPos5.AutoSize = true;
            this.lblPos5.Location = new System.Drawing.Point(105, 237);
            this.lblPos5.Name = "lblPos5";
            this.lblPos5.Size = new System.Drawing.Size(53, 15);
            this.lblPos5.TabIndex = 2;
            this.lblPos5.Text = "( 0 , 0 )";
            // 
            // txtTimer1
            // 
            this.txtTimer1.BackColor = System.Drawing.Color.DarkGray;
            this.txtTimer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTimer1.Location = new System.Drawing.Point(309, 126);
            this.txtTimer1.Name = "txtTimer1";
            this.txtTimer1.Size = new System.Drawing.Size(131, 22);
            this.txtTimer1.TabIndex = 100;
            this.txtTimer1.TextChanged += new System.EventHandler(this.txtTimer1_TextChanged);
            this.txtTimer1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimer1_KeyDown);
            this.txtTimer1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTimer1_KeyPress);
            this.txtTimer1.MouseHover += new System.EventHandler(this.txtTimer1_MouseHover);
            // 
            // txtTimer2
            // 
            this.txtTimer2.BackColor = System.Drawing.Color.DarkGray;
            this.txtTimer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTimer2.Location = new System.Drawing.Point(309, 153);
            this.txtTimer2.Name = "txtTimer2";
            this.txtTimer2.Size = new System.Drawing.Size(131, 22);
            this.txtTimer2.TabIndex = 101;
            this.txtTimer2.TextChanged += new System.EventHandler(this.txtTimer2_TextChanged);
            this.txtTimer2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimer2_KeyDown);
            this.txtTimer2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTimer2_KeyPress);
            this.txtTimer2.MouseHover += new System.EventHandler(this.txtTimer1_MouseHover);
            // 
            // txtTimer3
            // 
            this.txtTimer3.BackColor = System.Drawing.Color.DarkGray;
            this.txtTimer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTimer3.Location = new System.Drawing.Point(309, 180);
            this.txtTimer3.Name = "txtTimer3";
            this.txtTimer3.Size = new System.Drawing.Size(131, 22);
            this.txtTimer3.TabIndex = 102;
            this.txtTimer3.TextChanged += new System.EventHandler(this.txtTimer3_TextChanged);
            this.txtTimer3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimer3_KeyDown);
            this.txtTimer3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTimer3_KeyPress);
            this.txtTimer3.MouseHover += new System.EventHandler(this.txtTimer1_MouseHover);
            // 
            // txtTimer4
            // 
            this.txtTimer4.BackColor = System.Drawing.Color.DarkGray;
            this.txtTimer4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTimer4.Location = new System.Drawing.Point(309, 207);
            this.txtTimer4.Name = "txtTimer4";
            this.txtTimer4.Size = new System.Drawing.Size(131, 22);
            this.txtTimer4.TabIndex = 103;
            this.txtTimer4.TextChanged += new System.EventHandler(this.txtTimer4_TextChanged);
            this.txtTimer4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimer4_KeyDown);
            this.txtTimer4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTimer4_KeyPress);
            this.txtTimer4.MouseHover += new System.EventHandler(this.txtTimer1_MouseHover);
            // 
            // txtTimer5
            // 
            this.txtTimer5.BackColor = System.Drawing.Color.DarkGray;
            this.txtTimer5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTimer5.Location = new System.Drawing.Point(309, 234);
            this.txtTimer5.Name = "txtTimer5";
            this.txtTimer5.Size = new System.Drawing.Size(131, 22);
            this.txtTimer5.TabIndex = 104;
            this.txtTimer5.TextChanged += new System.EventHandler(this.txtTimer5_TextChanged);
            this.txtTimer5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTimer5_KeyDown);
            this.txtTimer5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTimer5_KeyPress);
            this.txtTimer5.MouseHover += new System.EventHandler(this.txtTimer1_MouseHover);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("NanumGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStart.Location = new System.Drawing.Point(16, 38);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(186, 40);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            this.btnStart.MouseHover += new System.EventHandler(this.btnStart_MouseHover);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(441, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "ms";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(441, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "ms";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(441, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "ms";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(441, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 15);
            this.label9.TabIndex = 2;
            this.label9.Text = "ms";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(441, 237);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 15);
            this.label10.TabIndex = 2;
            this.label10.Text = "ms";
            // 
            // lblUse
            // 
            this.lblUse.AutoSize = true;
            this.lblUse.Location = new System.Drawing.Point(21, 95);
            this.lblUse.Name = "lblUse";
            this.lblUse.Size = new System.Drawing.Size(30, 15);
            this.lblUse.TabIndex = 2;
            this.lblUse.Text = "USE";
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(107, 96);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(56, 15);
            this.lblPosition.TabIndex = 2;
            this.lblPosition.Text = "Position";
            // 
            // lblPositionSet
            // 
            this.lblPositionSet.AutoSize = true;
            this.lblPositionSet.Location = new System.Drawing.Point(213, 96);
            this.lblPositionSet.Name = "lblPositionSet";
            this.lblPositionSet.Size = new System.Drawing.Size(79, 15);
            this.lblPositionSet.TabIndex = 2;
            this.lblPositionSet.Text = "Position Set";
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(309, 96);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(41, 15);
            this.lblTimer.TabIndex = 2;
            this.lblTimer.Text = "Timer";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.lblMPos);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.lblPosition);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lblUse);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.lblPositionSet);
            this.panel1.Controls.Add(this.txtTimer5);
            this.panel1.Controls.Add(this.lblTimer);
            this.panel1.Controls.Add(this.cbCount);
            this.panel1.Controls.Add(this.cbPos1);
            this.panel1.Controls.Add(this.txtTimer4);
            this.panel1.Controls.Add(this.cbPos2);
            this.panel1.Controls.Add(this.txtTimer3);
            this.panel1.Controls.Add(this.cbPos3);
            this.panel1.Controls.Add(this.txtTimer2);
            this.panel1.Controls.Add(this.cbPos4);
            this.panel1.Controls.Add(this.txtCount);
            this.panel1.Controls.Add(this.txtTimer1);
            this.panel1.Controls.Add(this.cbPos5);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.btnPosSet1);
            this.panel1.Controls.Add(this.lblPos5);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btnPosSet2);
            this.panel1.Controls.Add(this.lblPos4);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.btnPosSet3);
            this.panel1.Controls.Add(this.lblPos3);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btnPosSet4);
            this.panel1.Controls.Add(this.lblPos2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnPosSet5);
            this.panel1.Controls.Add(this.lblPos1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(478, 267);
            this.panel1.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DimGray;
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.btnMinimize);
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(478, 26);
            this.panel4.TabIndex = 1114;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Font = new System.Drawing.Font("NanumGothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 15);
            this.label1.TabIndex = 1113;
            this.label1.Text = "Auto Click Program v.3.23";
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.DimGray;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Gulim", 10F, System.Drawing.FontStyle.Bold);
            this.btnMinimize.ForeColor = System.Drawing.Color.White;
            this.btnMinimize.Location = new System.Drawing.Point(421, 0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(25, 25);
            this.btnMinimize.TabIndex = 1112;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.Text = "_";
            this.btnMinimize.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.DimGray;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Gulim", 10F, System.Drawing.FontStyle.Bold);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(449, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(25, 25);
            this.btnClose.TabIndex = 1111;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "×";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "English",
            "한국어"});
            this.comboBox1.Location = new System.Drawing.Point(342, 55);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 23);
            this.comboBox1.TabIndex = 1113;
            this.comboBox1.Text = "English";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblMPos
            // 
            this.lblMPos.AutoSize = true;
            this.lblMPos.Location = new System.Drawing.Point(105, 129);
            this.lblMPos.Name = "lblMPos";
            this.lblMPos.Size = new System.Drawing.Size(53, 15);
            this.lblMPos.TabIndex = 2;
            this.lblMPos.Text = "( 0 , 0 )";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel3.Location = new System.Drawing.Point(16, 91);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(450, 1);
            this.panel3.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel2.Location = new System.Drawing.Point(16, 116);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(449, 1);
            this.panel2.TabIndex = 4;
            // 
            // cbCount
            // 
            this.cbCount.AutoSize = true;
            this.cbCount.Location = new System.Drawing.Point(216, 38);
            this.cbCount.Name = "cbCount";
            this.cbCount.Size = new System.Drawing.Size(64, 19);
            this.cbCount.TabIndex = 0;
            this.cbCount.Text = "Count";
            this.cbCount.UseVisualStyleBackColor = true;
            this.cbCount.CheckedChanged += new System.EventHandler(this.cbCount_CheckedChanged);
            // 
            // txtCount
            // 
            this.txtCount.BackColor = System.Drawing.Color.DarkGray;
            this.txtCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCount.Location = new System.Drawing.Point(216, 56);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(100, 22);
            this.txtCount.TabIndex = 3;
            this.txtCount.TextChanged += new System.EventHandler(this.txtCount_TextChanged);
            this.txtCount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCount_KeyDown);
            this.txtCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCount_KeyPress);
            // 
            // lblToolTip
            // 
            this.lblToolTip.AutoSize = true;
            this.lblToolTip.Font = new System.Drawing.Font("NanumGothic", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblToolTip.Location = new System.Drawing.Point(3, 271);
            this.lblToolTip.Name = "lblToolTip";
            this.lblToolTip.Size = new System.Drawing.Size(432, 14);
            this.lblToolTip.TabIndex = 2;
            this.lblToolTip.Text = "Click Position Set button and Move Mouse Position then Press the SPACE BAR";
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(480, 289);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblToolTip);
            this.Font = new System.Drawing.Font("NanumGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.Name = "fMain";
            this.Text = "Auto Click Program v.3.23";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fMain_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbPos1;
        private System.Windows.Forms.CheckBox cbPos2;
        private System.Windows.Forms.CheckBox cbPos3;
        private System.Windows.Forms.CheckBox cbPos4;
        private System.Windows.Forms.CheckBox cbPos5;
        private System.Windows.Forms.Button btnPosSet1;
        private System.Windows.Forms.Button btnPosSet2;
        private System.Windows.Forms.Button btnPosSet3;
        private System.Windows.Forms.Button btnPosSet4;
        private System.Windows.Forms.Button btnPosSet5;
        private System.Windows.Forms.Label lblPos1;
        private System.Windows.Forms.Label lblPos2;
        private System.Windows.Forms.Label lblPos3;
        private System.Windows.Forms.Label lblPos4;
        private System.Windows.Forms.Label lblPos5;
        private System.Windows.Forms.TextBox txtTimer1;
        private System.Windows.Forms.TextBox txtTimer2;
        private System.Windows.Forms.TextBox txtTimer3;
        private System.Windows.Forms.TextBox txtTimer4;
        private System.Windows.Forms.TextBox txtTimer5;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblUse;
        private System.Windows.Forms.Label lblPosition;
        private System.Windows.Forms.Label lblPositionSet;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblMPos;
        private System.Windows.Forms.Label lblToolTip;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox cbCount;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
    }
}

