﻿namespace AccountBook
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.tcMain = new System.Windows.Forms.TabControl();
            this.t1Home = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblToday = new System.Windows.Forms.TextBox();
            this.t1gbAccountInfo = new System.Windows.Forms.GroupBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.dvAccountList = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.t1gbExport = new System.Windows.Forms.GroupBox();
            this.txtMExport = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dvExportList = new System.Windows.Forms.DataGridView();
            this.lblE = new System.Windows.Forms.Label();
            this.t1gbImport = new System.Windows.Forms.GroupBox();
            this.txtMImport = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dvImportList = new System.Windows.Forms.DataGridView();
            this.lblI = new System.Windows.Forms.Label();
            this.t1gbAccount = new System.Windows.Forms.GroupBox();
            this.txtTotalAccount = new System.Windows.Forms.TextBox();
            this.txtTotalExport = new System.Windows.Forms.TextBox();
            this.txtTotalImport = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.t2AccountBook = new System.Windows.Forms.TabPage();
            this.t2gbAccountBook = new System.Windows.Forms.GroupBox();
            this.lblCreditTerm = new System.Windows.Forms.Label();
            this.txtCreditAuto = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUseDate = new System.Windows.Forms.DateTimePicker();
            this.btnSet = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtUseCost = new System.Windows.Forms.TextBox();
            this.cbIEStandard = new System.Windows.Forms.ComboBox();
            this.cbIEType = new System.Windows.Forms.ComboBox();
            this.cbAccount = new System.Windows.Forms.ComboBox();
            this.cbUseType = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dvEdit = new System.Windows.Forms.DataGridView();
            this.txtUseDate1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbUseType1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbAccount1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbIEType1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbIEStandard1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.txtCost1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRemarks1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSet1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dvAccountBook = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.lblNowMonth = new System.Windows.Forms.Label();
            this.t3Setting = new System.Windows.Forms.TabPage();
            this.btnSettingSave = new System.Windows.Forms.Button();
            this.t3gbStep4 = new System.Windows.Forms.GroupBox();
            this.dvCreditInfo = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.t3gbStep3 = new System.Windows.Forms.GroupBox();
            this.dvAccountInfo = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.t3gbStep2 = new System.Windows.Forms.GroupBox();
            this.dvSettingInfo = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.tcMain.SuspendLayout();
            this.t1Home.SuspendLayout();
            this.t1gbAccountInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvAccountList)).BeginInit();
            this.t1gbExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvExportList)).BeginInit();
            this.t1gbImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvImportList)).BeginInit();
            this.t1gbAccount.SuspendLayout();
            this.t2AccountBook.SuspendLayout();
            this.t2gbAccountBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvAccountBook)).BeginInit();
            this.t3Setting.SuspendLayout();
            this.t3gbStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvCreditInfo)).BeginInit();
            this.t3gbStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvAccountInfo)).BeginInit();
            this.t3gbStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvSettingInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.t1Home);
            this.tcMain.Controls.Add(this.t2AccountBook);
            this.tcMain.Controls.Add(this.t3Setting);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1227, 639);
            this.tcMain.TabIndex = 0;
            // 
            // t1Home
            // 
            this.t1Home.Controls.Add(this.textBox1);
            this.t1Home.Controls.Add(this.lblToday);
            this.t1Home.Controls.Add(this.t1gbAccountInfo);
            this.t1Home.Controls.Add(this.t1gbExport);
            this.t1Home.Controls.Add(this.t1gbImport);
            this.t1Home.Controls.Add(this.t1gbAccount);
            this.t1Home.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t1Home.Location = new System.Drawing.Point(4, 23);
            this.t1Home.Name = "t1Home";
            this.t1Home.Padding = new System.Windows.Forms.Padding(3);
            this.t1Home.Size = new System.Drawing.Size(1219, 612);
            this.t1Home.TabIndex = 0;
            this.t1Home.Text = "HOME";
            this.t1Home.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(13, 6);
            this.textBox1.Margin = new System.Windows.Forms.Padding(0);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(329, 23);
            this.textBox1.TabIndex = 17;
            this.textBox1.Text = "J.Ella\'s Account Book";
            // 
            // lblToday
            // 
            this.lblToday.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblToday.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToday.Location = new System.Drawing.Point(882, 8);
            this.lblToday.Margin = new System.Windows.Forms.Padding(0);
            this.lblToday.Name = "lblToday";
            this.lblToday.Size = new System.Drawing.Size(329, 23);
            this.lblToday.TabIndex = 16;
            this.lblToday.Text = "today";
            this.lblToday.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // t1gbAccountInfo
            // 
            this.t1gbAccountInfo.Controls.Add(this.txtTotal);
            this.t1gbAccountInfo.Controls.Add(this.label23);
            this.t1gbAccountInfo.Controls.Add(this.dvAccountList);
            this.t1gbAccountInfo.Controls.Add(this.label9);
            this.t1gbAccountInfo.Location = new System.Drawing.Point(3, 230);
            this.t1gbAccountInfo.Name = "t1gbAccountInfo";
            this.t1gbAccountInfo.Size = new System.Drawing.Size(493, 374);
            this.t1gbAccountInfo.TabIndex = 2;
            this.t1gbAccountInfo.TabStop = false;
            this.t1gbAccountInfo.Text = "Account Balance";
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.White;
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotal.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(301, 40);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(0);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(181, 23);
            this.txtTotal.TabIndex = 15;
            this.txtTotal.Text = "1,000,000";
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 22);
            this.label23.TabIndex = 10;
            this.label23.Text = "TOTAL";
            // 
            // dvAccountList
            // 
            this.dvAccountList.AllowUserToAddRows = false;
            this.dvAccountList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvAccountList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvAccountList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvAccountList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvAccountList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvAccountList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvAccountList.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvAccountList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dvAccountList.EnableHeadersVisualStyles = false;
            this.dvAccountList.Location = new System.Drawing.Point(9, 69);
            this.dvAccountList.Name = "dvAccountList";
            this.dvAccountList.ReadOnly = true;
            this.dvAccountList.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dvAccountList.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dvAccountList.RowTemplate.Height = 23;
            this.dvAccountList.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dvAccountList.Size = new System.Drawing.Size(478, 297);
            this.dvAccountList.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 18);
            this.label9.TabIndex = 7;
            this.label9.Text = "통장 잔액";
            // 
            // t1gbExport
            // 
            this.t1gbExport.Controls.Add(this.txtMExport);
            this.t1gbExport.Controls.Add(this.label15);
            this.t1gbExport.Controls.Add(this.dvExportList);
            this.t1gbExport.Controls.Add(this.lblE);
            this.t1gbExport.Location = new System.Drawing.Point(861, 34);
            this.t1gbExport.Name = "t1gbExport";
            this.t1gbExport.Size = new System.Drawing.Size(350, 570);
            this.t1gbExport.TabIndex = 2;
            this.t1gbExport.TabStop = false;
            this.t1gbExport.Text = "Export";
            // 
            // txtMExport
            // 
            this.txtMExport.BackColor = System.Drawing.Color.White;
            this.txtMExport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMExport.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMExport.Location = new System.Drawing.Point(155, 40);
            this.txtMExport.Margin = new System.Windows.Forms.Padding(0);
            this.txtMExport.Name = "txtMExport";
            this.txtMExport.ReadOnly = true;
            this.txtMExport.Size = new System.Drawing.Size(181, 23);
            this.txtMExport.TabIndex = 15;
            this.txtMExport.Text = "1,000,000";
            this.txtMExport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 22);
            this.label15.TabIndex = 8;
            this.label15.Text = "EXPORT";
            // 
            // dvExportList
            // 
            this.dvExportList.AllowUserToAddRows = false;
            this.dvExportList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvExportList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvExportList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvExportList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvExportList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvExportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvExportList.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvExportList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dvExportList.EnableHeadersVisualStyles = false;
            this.dvExportList.Location = new System.Drawing.Point(6, 69);
            this.dvExportList.Name = "dvExportList";
            this.dvExportList.ReadOnly = true;
            this.dvExportList.RowHeadersVisible = false;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dvExportList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dvExportList.RowTemplate.Height = 23;
            this.dvExportList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dvExportList.Size = new System.Drawing.Size(335, 493);
            this.dvExportList.TabIndex = 7;
            // 
            // lblE
            // 
            this.lblE.AutoSize = true;
            this.lblE.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblE.Location = new System.Drawing.Point(6, 18);
            this.lblE.Name = "lblE";
            this.lblE.Size = new System.Drawing.Size(112, 18);
            this.lblE.TabIndex = 6;
            this.lblE.Text = "이번달 계정별 지출";
            // 
            // t1gbImport
            // 
            this.t1gbImport.Controls.Add(this.txtMImport);
            this.t1gbImport.Controls.Add(this.label14);
            this.t1gbImport.Controls.Add(this.dvImportList);
            this.t1gbImport.Controls.Add(this.lblI);
            this.t1gbImport.Location = new System.Drawing.Point(505, 34);
            this.t1gbImport.Name = "t1gbImport";
            this.t1gbImport.Size = new System.Drawing.Size(350, 570);
            this.t1gbImport.TabIndex = 1;
            this.t1gbImport.TabStop = false;
            this.t1gbImport.Text = "Import";
            // 
            // txtMImport
            // 
            this.txtMImport.BackColor = System.Drawing.Color.White;
            this.txtMImport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMImport.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMImport.Location = new System.Drawing.Point(158, 40);
            this.txtMImport.Margin = new System.Windows.Forms.Padding(0);
            this.txtMImport.Name = "txtMImport";
            this.txtMImport.ReadOnly = true;
            this.txtMImport.Size = new System.Drawing.Size(181, 23);
            this.txtMImport.TabIndex = 15;
            this.txtMImport.Text = "1,000,000";
            this.txtMImport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 22);
            this.label14.TabIndex = 7;
            this.label14.Text = "IMPORT";
            // 
            // dvImportList
            // 
            this.dvImportList.AllowUserToAddRows = false;
            this.dvImportList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvImportList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvImportList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvImportList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvImportList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvImportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvImportList.ColumnHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvImportList.DefaultCellStyle = dataGridViewCellStyle5;
            this.dvImportList.EnableHeadersVisualStyles = false;
            this.dvImportList.Location = new System.Drawing.Point(9, 69);
            this.dvImportList.Name = "dvImportList";
            this.dvImportList.ReadOnly = true;
            this.dvImportList.RowHeadersVisible = false;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dvImportList.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dvImportList.RowTemplate.Height = 23;
            this.dvImportList.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dvImportList.Size = new System.Drawing.Size(335, 493);
            this.dvImportList.TabIndex = 6;
            // 
            // lblI
            // 
            this.lblI.AutoSize = true;
            this.lblI.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblI.Location = new System.Drawing.Point(6, 18);
            this.lblI.Name = "lblI";
            this.lblI.Size = new System.Drawing.Size(112, 18);
            this.lblI.TabIndex = 5;
            this.lblI.Text = "이번달 계정별 수입";
            // 
            // t1gbAccount
            // 
            this.t1gbAccount.Controls.Add(this.txtTotalAccount);
            this.t1gbAccount.Controls.Add(this.txtTotalExport);
            this.t1gbAccount.Controls.Add(this.txtTotalImport);
            this.t1gbAccount.Controls.Add(this.label24);
            this.t1gbAccount.Controls.Add(this.label20);
            this.t1gbAccount.Controls.Add(this.label19);
            this.t1gbAccount.Controls.Add(this.label5);
            this.t1gbAccount.Location = new System.Drawing.Point(6, 34);
            this.t1gbAccount.Name = "t1gbAccount";
            this.t1gbAccount.Size = new System.Drawing.Size(493, 190);
            this.t1gbAccount.TabIndex = 0;
            this.t1gbAccount.TabStop = false;
            this.t1gbAccount.Text = "Main";
            // 
            // txtTotalAccount
            // 
            this.txtTotalAccount.BackColor = System.Drawing.Color.White;
            this.txtTotalAccount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalAccount.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAccount.ForeColor = System.Drawing.Color.Black;
            this.txtTotalAccount.Location = new System.Drawing.Point(298, 143);
            this.txtTotalAccount.Margin = new System.Windows.Forms.Padding(0);
            this.txtTotalAccount.Name = "txtTotalAccount";
            this.txtTotalAccount.ReadOnly = true;
            this.txtTotalAccount.Size = new System.Drawing.Size(181, 25);
            this.txtTotalAccount.TabIndex = 14;
            this.txtTotalAccount.Text = "1,000,000";
            this.txtTotalAccount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalExport
            // 
            this.txtTotalExport.BackColor = System.Drawing.Color.White;
            this.txtTotalExport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalExport.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalExport.Location = new System.Drawing.Point(298, 85);
            this.txtTotalExport.Margin = new System.Windows.Forms.Padding(0);
            this.txtTotalExport.Name = "txtTotalExport";
            this.txtTotalExport.ReadOnly = true;
            this.txtTotalExport.Size = new System.Drawing.Size(181, 23);
            this.txtTotalExport.TabIndex = 14;
            this.txtTotalExport.Text = "1,000,000";
            this.txtTotalExport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalImport
            // 
            this.txtTotalImport.BackColor = System.Drawing.Color.White;
            this.txtTotalImport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalImport.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalImport.Location = new System.Drawing.Point(298, 49);
            this.txtTotalImport.Margin = new System.Windows.Forms.Padding(0);
            this.txtTotalImport.Name = "txtTotalImport";
            this.txtTotalImport.ReadOnly = true;
            this.txtTotalImport.Size = new System.Drawing.Size(181, 23);
            this.txtTotalImport.TabIndex = 14;
            this.txtTotalImport.Text = "1,000,000";
            this.txtTotalImport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(6, 145);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 22);
            this.label24.TabIndex = 12;
            this.label24.Text = "TOTAL";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 85);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 22);
            this.label20.TabIndex = 10;
            this.label20.Text = "EXPORT";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 49);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 22);
            this.label19.TabIndex = 9;
            this.label19.Text = "IMPORT";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 18);
            this.label5.TabIndex = 3;
            this.label5.Text = "누적 수입 / 지출 현황";
            // 
            // t2AccountBook
            // 
            this.t2AccountBook.Controls.Add(this.t2gbAccountBook);
            this.t2AccountBook.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t2AccountBook.Location = new System.Drawing.Point(4, 23);
            this.t2AccountBook.Name = "t2AccountBook";
            this.t2AccountBook.Padding = new System.Windows.Forms.Padding(3);
            this.t2AccountBook.Size = new System.Drawing.Size(1219, 612);
            this.t2AccountBook.TabIndex = 1;
            this.t2AccountBook.Text = "Account Book";
            this.t2AccountBook.UseVisualStyleBackColor = true;
            // 
            // t2gbAccountBook
            // 
            this.t2gbAccountBook.Controls.Add(this.lblCreditTerm);
            this.t2gbAccountBook.Controls.Add(this.txtCreditAuto);
            this.t2gbAccountBook.Controls.Add(this.label6);
            this.t2gbAccountBook.Controls.Add(this.txtUseDate);
            this.t2gbAccountBook.Controls.Add(this.btnSet);
            this.t2gbAccountBook.Controls.Add(this.txtRemarks);
            this.t2gbAccountBook.Controls.Add(this.txtUseCost);
            this.t2gbAccountBook.Controls.Add(this.cbIEStandard);
            this.t2gbAccountBook.Controls.Add(this.cbIEType);
            this.t2gbAccountBook.Controls.Add(this.cbAccount);
            this.t2gbAccountBook.Controls.Add(this.cbUseType);
            this.t2gbAccountBook.Controls.Add(this.label13);
            this.t2gbAccountBook.Controls.Add(this.dvEdit);
            this.t2gbAccountBook.Controls.Add(this.dvAccountBook);
            this.t2gbAccountBook.Controls.Add(this.label10);
            this.t2gbAccountBook.Controls.Add(this.lblNowMonth);
            this.t2gbAccountBook.Location = new System.Drawing.Point(8, 6);
            this.t2gbAccountBook.Name = "t2gbAccountBook";
            this.t2gbAccountBook.Size = new System.Drawing.Size(1203, 600);
            this.t2gbAccountBook.TabIndex = 1;
            this.t2gbAccountBook.TabStop = false;
            this.t2gbAccountBook.Text = "Account Book";
            // 
            // lblCreditTerm
            // 
            this.lblCreditTerm.AutoSize = true;
            this.lblCreditTerm.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTerm.Location = new System.Drawing.Point(959, 16);
            this.lblCreditTerm.Name = "lblCreditTerm";
            this.lblCreditTerm.Size = new System.Drawing.Size(52, 18);
            this.lblCreditTerm.TabIndex = 13;
            this.lblCreditTerm.Text = "사용기간";
            // 
            // txtCreditAuto
            // 
            this.txtCreditAuto.Enabled = false;
            this.txtCreditAuto.Location = new System.Drawing.Point(1049, 40);
            this.txtCreditAuto.Name = "txtCreditAuto";
            this.txtCreditAuto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCreditAuto.Size = new System.Drawing.Size(148, 22);
            this.txtCreditAuto.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(846, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(197, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "신용카드 사용내역 자동계산 출금액";
            // 
            // txtUseDate
            // 
            this.txtUseDate.CustomFormat = "yyyy-MM-dd";
            this.txtUseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtUseDate.Location = new System.Drawing.Point(10, 97);
            this.txtUseDate.Name = "txtUseDate";
            this.txtUseDate.Size = new System.Drawing.Size(148, 22);
            this.txtUseDate.TabIndex = 1;
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(1048, 97);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(147, 22);
            this.btnSet.TabIndex = 8;
            this.btnSet.Text = "입력";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(900, 97);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(148, 22);
            this.txtRemarks.TabIndex = 7;
            // 
            // txtUseCost
            // 
            this.txtUseCost.Location = new System.Drawing.Point(752, 97);
            this.txtUseCost.Name = "txtUseCost";
            this.txtUseCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtUseCost.Size = new System.Drawing.Size(148, 22);
            this.txtUseCost.TabIndex = 6;
            // 
            // cbIEStandard
            // 
            this.cbIEStandard.FormattingEnabled = true;
            this.cbIEStandard.Location = new System.Drawing.Point(603, 97);
            this.cbIEStandard.Name = "cbIEStandard";
            this.cbIEStandard.Size = new System.Drawing.Size(149, 22);
            this.cbIEStandard.TabIndex = 5;
            // 
            // cbIEType
            // 
            this.cbIEType.FormattingEnabled = true;
            this.cbIEType.Location = new System.Drawing.Point(455, 97);
            this.cbIEType.Name = "cbIEType";
            this.cbIEType.Size = new System.Drawing.Size(148, 22);
            this.cbIEType.TabIndex = 4;
            this.cbIEType.SelectedIndexChanged += new System.EventHandler(this.cbIEType_SelectedIndexChanged);
            // 
            // cbAccount
            // 
            this.cbAccount.FormattingEnabled = true;
            this.cbAccount.Location = new System.Drawing.Point(306, 97);
            this.cbAccount.Name = "cbAccount";
            this.cbAccount.Size = new System.Drawing.Size(149, 22);
            this.cbAccount.TabIndex = 3;
            // 
            // cbUseType
            // 
            this.cbUseType.FormattingEnabled = true;
            this.cbUseType.Location = new System.Drawing.Point(158, 97);
            this.cbUseType.Name = "cbUseType";
            this.cbUseType.Size = new System.Drawing.Size(148, 22);
            this.cbUseType.TabIndex = 2;
            this.cbUseType.SelectedIndexChanged += new System.EventHandler(this.cbUseType_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 18);
            this.label13.TabIndex = 9;
            this.label13.Text = "상세내역 입력";
            // 
            // dvEdit
            // 
            this.dvEdit.AllowUserToAddRows = false;
            this.dvEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvEdit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvEdit.BackgroundColor = System.Drawing.Color.LightGray;
            this.dvEdit.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvEdit.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvEdit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtUseDate1,
            this.cbUseType1,
            this.cbAccount1,
            this.cbIEType1,
            this.cbIEStandard1,
            this.txtCost1,
            this.txtRemarks1,
            this.btnSet1});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvEdit.DefaultCellStyle = dataGridViewCellStyle10;
            this.dvEdit.Enabled = false;
            this.dvEdit.EnableHeadersVisualStyles = false;
            this.dvEdit.Location = new System.Drawing.Point(9, 72);
            this.dvEdit.Name = "dvEdit";
            this.dvEdit.RowHeadersVisible = false;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.dvEdit.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dvEdit.RowTemplate.Height = 23;
            this.dvEdit.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dvEdit.Size = new System.Drawing.Size(1188, 49);
            this.dvEdit.TabIndex = 8;
            // 
            // txtUseDate1
            // 
            dataGridViewCellStyle7.Format = "M";
            dataGridViewCellStyle7.NullValue = null;
            this.txtUseDate1.DefaultCellStyle = dataGridViewCellStyle7;
            this.txtUseDate1.HeaderText = "날짜";
            this.txtUseDate1.Name = "txtUseDate1";
            // 
            // cbUseType1
            // 
            this.cbUseType1.HeaderText = "구분";
            this.cbUseType1.Items.AddRange(new object[] {
            "수입",
            "지출",
            "계좌이동"});
            this.cbUseType1.Name = "cbUseType1";
            this.cbUseType1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbUseType1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbAccount1
            // 
            this.cbAccount1.HeaderText = "계정";
            this.cbAccount1.Name = "cbAccount1";
            this.cbAccount1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbAccount1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbIEType1
            // 
            this.cbIEType1.HeaderText = "입/출금 구분";
            this.cbIEType1.Name = "cbIEType1";
            this.cbIEType1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbIEType1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbIEStandard1
            // 
            this.cbIEStandard1.HeaderText = "입/출금 기준";
            this.cbIEStandard1.Name = "cbIEStandard1";
            this.cbIEStandard1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbIEStandard1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txtCost1
            // 
            dataGridViewCellStyle8.Format = "C0";
            dataGridViewCellStyle8.NullValue = null;
            this.txtCost1.DefaultCellStyle = dataGridViewCellStyle8;
            this.txtCost1.HeaderText = "금액";
            this.txtCost1.Name = "txtCost1";
            // 
            // txtRemarks1
            // 
            this.txtRemarks1.HeaderText = "상세 내역 및 비고";
            this.txtRemarks1.Name = "txtRemarks1";
            // 
            // btnSet1
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.NullValue = "입력";
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.btnSet1.DefaultCellStyle = dataGridViewCellStyle9;
            this.btnSet1.HeaderText = "입력";
            this.btnSet1.Name = "btnSet1";
            this.btnSet1.ReadOnly = true;
            this.btnSet1.Text = "입력";
            this.btnSet1.UseColumnTextForButtonValue = true;
            // 
            // dvAccountBook
            // 
            this.dvAccountBook.AllowUserToAddRows = false;
            this.dvAccountBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvAccountBook.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvAccountBook.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvAccountBook.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvAccountBook.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvAccountBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvAccountBook.DefaultCellStyle = dataGridViewCellStyle12;
            this.dvAccountBook.EnableHeadersVisualStyles = false;
            this.dvAccountBook.Location = new System.Drawing.Point(9, 141);
            this.dvAccountBook.Name = "dvAccountBook";
            this.dvAccountBook.RowHeadersVisible = false;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.dvAccountBook.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dvAccountBook.RowTemplate.Height = 23;
            this.dvAccountBook.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dvAccountBook.Size = new System.Drawing.Size(1188, 453);
            this.dvAccountBook.TabIndex = 10;
            this.dvAccountBook.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dvAccountBook_MouseDoubleClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 18);
            this.label10.TabIndex = 4;
            this.label10.Text = "가계부 상세내역";
            // 
            // lblNowMonth
            // 
            this.lblNowMonth.AutoSize = true;
            this.lblNowMonth.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNowMonth.Location = new System.Drawing.Point(848, 13);
            this.lblNowMonth.Name = "lblNowMonth";
            this.lblNowMonth.Size = new System.Drawing.Size(60, 22);
            this.lblNowMonth.TabIndex = 3;
            this.lblNowMonth.Text = "Month";
            // 
            // t3Setting
            // 
            this.t3Setting.Controls.Add(this.btnSettingSave);
            this.t3Setting.Controls.Add(this.t3gbStep4);
            this.t3Setting.Controls.Add(this.t3gbStep3);
            this.t3Setting.Controls.Add(this.t3gbStep2);
            this.t3Setting.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t3Setting.Location = new System.Drawing.Point(4, 23);
            this.t3Setting.Name = "t3Setting";
            this.t3Setting.Size = new System.Drawing.Size(1219, 612);
            this.t3Setting.TabIndex = 2;
            this.t3Setting.Text = "Setting";
            this.t3Setting.UseVisualStyleBackColor = true;
            // 
            // btnSettingSave
            // 
            this.btnSettingSave.Location = new System.Drawing.Point(1082, 581);
            this.btnSettingSave.Name = "btnSettingSave";
            this.btnSettingSave.Size = new System.Drawing.Size(129, 23);
            this.btnSettingSave.TabIndex = 4;
            this.btnSettingSave.Text = "SAVE SETTINGS";
            this.btnSettingSave.UseVisualStyleBackColor = true;
            this.btnSettingSave.Click += new System.EventHandler(this.btnSettingSave_Click);
            // 
            // t3gbStep4
            // 
            this.t3gbStep4.Controls.Add(this.dvCreditInfo);
            this.t3gbStep4.Controls.Add(this.label4);
            this.t3gbStep4.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t3gbStep4.Location = new System.Drawing.Point(408, 342);
            this.t3gbStep4.Name = "t3gbStep4";
            this.t3gbStep4.Size = new System.Drawing.Size(803, 233);
            this.t3gbStep4.TabIndex = 2;
            this.t3gbStep4.TabStop = false;
            this.t3gbStep4.Text = "Step 3. ";
            // 
            // dvCreditInfo
            // 
            this.dvCreditInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvCreditInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvCreditInfo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvCreditInfo.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvCreditInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvCreditInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvCreditInfo.DefaultCellStyle = dataGridViewCellStyle14;
            this.dvCreditInfo.EnableHeadersVisualStyles = false;
            this.dvCreditInfo.Location = new System.Drawing.Point(6, 39);
            this.dvCreditInfo.Name = "dvCreditInfo";
            this.dvCreditInfo.RowHeadersVisible = false;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dvCreditInfo.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dvCreditInfo.RowTemplate.Height = 23;
            this.dvCreditInfo.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dvCreditInfo.Size = new System.Drawing.Size(791, 188);
            this.dvCreditInfo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "신용카드 정보 입력";
            // 
            // t3gbStep3
            // 
            this.t3gbStep3.Controls.Add(this.dvAccountInfo);
            this.t3gbStep3.Controls.Add(this.label3);
            this.t3gbStep3.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t3gbStep3.Location = new System.Drawing.Point(408, 3);
            this.t3gbStep3.Name = "t3gbStep3";
            this.t3gbStep3.Size = new System.Drawing.Size(803, 333);
            this.t3gbStep3.TabIndex = 1;
            this.t3gbStep3.TabStop = false;
            this.t3gbStep3.Text = "Step 2. ";
            // 
            // dvAccountInfo
            // 
            this.dvAccountInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvAccountInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvAccountInfo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvAccountInfo.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvAccountInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvAccountInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvAccountInfo.DefaultCellStyle = dataGridViewCellStyle16;
            this.dvAccountInfo.EnableHeadersVisualStyles = false;
            this.dvAccountInfo.Location = new System.Drawing.Point(6, 41);
            this.dvAccountInfo.Name = "dvAccountInfo";
            this.dvAccountInfo.RowHeadersVisible = false;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dvAccountInfo.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dvAccountInfo.RowTemplate.Height = 23;
            this.dvAccountInfo.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dvAccountInfo.Size = new System.Drawing.Size(791, 286);
            this.dvAccountInfo.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "보유 계좌 및 체크카드 정보 입력";
            // 
            // t3gbStep2
            // 
            this.t3gbStep2.Controls.Add(this.dvSettingInfo);
            this.t3gbStep2.Controls.Add(this.label2);
            this.t3gbStep2.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t3gbStep2.Location = new System.Drawing.Point(8, 3);
            this.t3gbStep2.Name = "t3gbStep2";
            this.t3gbStep2.Size = new System.Drawing.Size(394, 601);
            this.t3gbStep2.TabIndex = 1;
            this.t3gbStep2.TabStop = false;
            this.t3gbStep2.Text = "Step 1. ";
            // 
            // dvSettingInfo
            // 
            this.dvSettingInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvSettingInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvSettingInfo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dvSettingInfo.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvSettingInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvSettingInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvSettingInfo.DefaultCellStyle = dataGridViewCellStyle18;
            this.dvSettingInfo.EnableHeadersVisualStyles = false;
            this.dvSettingInfo.Location = new System.Drawing.Point(6, 39);
            this.dvSettingInfo.Name = "dvSettingInfo";
            this.dvSettingInfo.RowHeadersVisible = false;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            this.dvSettingInfo.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dvSettingInfo.RowTemplate.Height = 23;
            this.dvSettingInfo.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dvSettingInfo.Size = new System.Drawing.Size(382, 556);
            this.dvSettingInfo.TabIndex = 5;
            this.dvSettingInfo.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvSettingInfo_CellValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "가계부 계정 항목 입력";
            // 
            // fMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1227, 639);
            this.Controls.Add(this.tcMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "J.Ella\'s Account Book";
            this.Load += new System.EventHandler(this.fMain_Load);
            this.tcMain.ResumeLayout(false);
            this.t1Home.ResumeLayout(false);
            this.t1Home.PerformLayout();
            this.t1gbAccountInfo.ResumeLayout(false);
            this.t1gbAccountInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvAccountList)).EndInit();
            this.t1gbExport.ResumeLayout(false);
            this.t1gbExport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvExportList)).EndInit();
            this.t1gbImport.ResumeLayout(false);
            this.t1gbImport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvImportList)).EndInit();
            this.t1gbAccount.ResumeLayout(false);
            this.t1gbAccount.PerformLayout();
            this.t2AccountBook.ResumeLayout(false);
            this.t2gbAccountBook.ResumeLayout(false);
            this.t2gbAccountBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvAccountBook)).EndInit();
            this.t3Setting.ResumeLayout(false);
            this.t3gbStep4.ResumeLayout(false);
            this.t3gbStep4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvCreditInfo)).EndInit();
            this.t3gbStep3.ResumeLayout(false);
            this.t3gbStep3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvAccountInfo)).EndInit();
            this.t3gbStep2.ResumeLayout(false);
            this.t3gbStep2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvSettingInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage t1Home;
        private System.Windows.Forms.TabPage t2AccountBook;
        private System.Windows.Forms.TabPage t3Setting;
        private System.Windows.Forms.GroupBox t3gbStep3;
        private System.Windows.Forms.GroupBox t3gbStep2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox t3gbStep4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dvSettingInfo;
        private System.Windows.Forms.DataGridView dvCreditInfo;
        private System.Windows.Forms.DataGridView dvAccountInfo;
        private System.Windows.Forms.GroupBox t1gbAccount;
        private System.Windows.Forms.GroupBox t1gbExport;
        private System.Windows.Forms.GroupBox t1gbImport;
        private System.Windows.Forms.Label lblE;
        private System.Windows.Forms.Label lblI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox t2gbAccountBook;
        private System.Windows.Forms.Label lblNowMonth;
        private System.Windows.Forms.DataGridView dvAccountBook;
        private System.Windows.Forms.DataGridView dvEdit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dvImportList;
        private System.Windows.Forms.DataGridView dvExportList;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnSettingSave;
        private System.Windows.Forms.TextBox txtUseCost;
        private System.Windows.Forms.ComboBox cbIEStandard;
        private System.Windows.Forms.ComboBox cbIEType;
        private System.Windows.Forms.ComboBox cbAccount;
        private System.Windows.Forms.ComboBox cbUseType;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtUseDate1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbUseType1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbAccount1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbIEType1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbIEStandard1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCost1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtRemarks1;
        private System.Windows.Forms.DataGridViewButtonColumn btnSet1;
        private System.Windows.Forms.DateTimePicker txtUseDate;
        private System.Windows.Forms.TextBox txtMExport;
        private System.Windows.Forms.TextBox txtMImport;
        private System.Windows.Forms.TextBox txtTotalAccount;
        private System.Windows.Forms.TextBox txtTotalExport;
        private System.Windows.Forms.TextBox txtTotalImport;
        private System.Windows.Forms.GroupBox t1gbAccountInfo;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView dvAccountList;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lblToday;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCreditAuto;
        private System.Windows.Forms.Label lblCreditTerm;
    }
}

