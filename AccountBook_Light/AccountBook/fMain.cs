﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccountBook
{
    public partial class fMain : Form
    {
        DBMySql _db = new DBMySql();
        fBookUpdate _bookUpdate;

        public static int cnt = 0;
        public static int bookcnt = 0;
        public int iTotalAccount, iTotalImport, iTotalExport, iCreditAuto, iTotalFirstAccount;
        public int iTotalMImport, iTotalMExport;
        public string Today = DateTime.Now.ToLongDateString();
        public string NowMonth = DateTime.Now.ToString("MMMM");
        public DateTime today = DateTime.Today;

        public fMain()
        {
            InitializeComponent();
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            //Main Screen Today Date
            lblToday.Text = Today;
            lblNowMonth.Text = NowMonth;

            //MySQL Database Connection
            _db.Connection();

            _bookUpdate = new fBookUpdate();
            
            InitSetting();
            InitAccountBook();
            InitMain();
        }

        public void InitSetting()
        {
            //DB Table 값 읽어와서 DataSet에 저장하고 각각의 DataGridView에 DataSource로 바인딩하기

            //setting_info 테이블 -> 수입계정, 지출계정
            Config.settingds = _db.SelectDetail(Config.Tables[(int)Config.eTName._setting], "import_info as '수입계정', export_info as '지출계정'");
            dvSettingInfo.DataSource = Config.settingds.Tables[0];
            cnt = 0;
            Config.ImportInfo = new string[Config.settingds.Tables[0].Rows.Count];
            Config.ExportInfo = new string[Config.settingds.Tables[0].Rows.Count];
            foreach (DataRow r in Config.settingds.Tables[0].Rows)
            {
                if (r["수입계정"] != DBNull.Value)
                {
                    Config.ImportInfo[cnt] = r["수입계정"].ToString();
                }
                if (r["지출계정"] != DBNull.Value)
                {
                    Config.ExportInfo[cnt] = r["지출계정"].ToString();
                }
                cnt++;
            }
            //데이터 Null값 제외하고 다시 정렬
            Config.ImportInfo = Config.ImportInfo.Where(c => c != null).ToArray();
            Config.ExportInfo = Config.ExportInfo.Where(c => c != null).ToArray();

            //account_info 테이블 -> 은행계좌정보 (+체크카드)
            cnt = 0;
            string AccStr = "bank_name as '은행명', account_name as '계좌명', connect_card as '연결체크카드', first_balance as '최초잔액', memo as '메모'";
            Config.accountds = _db.SelectDetail(Config.Tables[(int)Config.eTName._account], AccStr);
            dvAccountInfo.DataSource = Config.accountds.Tables[0];
            dvAccountInfo.Columns[3].DefaultCellStyle.Format = "###,##0";
            dvAccountInfo.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            Config.AccountName = new string[Config.accountds.Tables[0].Rows.Count];
            Config.ConnectCard = new string[Config.accountds.Tables[0].Rows.Count];
            foreach (DataRow r in Config.accountds.Tables[0].Rows)
            {
                Config.AccountName[cnt] = r["계좌명"].ToString();
                Config.ConnectCard[cnt] = r["연결체크카드"].ToString();
                iTotalFirstAccount += Convert.ToInt32(r["최초잔액"]);
                cnt++;
            }

            //credit_info 테이블 -> 신용카드정보
            cnt = 0;
            string CreditStr = "credit_bank as '신용카드사', card_name as '카드명', withdraw_account as '출금계좌', bank_name as '은행명', withdraw_date as '출금일', use_term as '사용실적기간'";
            Config.creditds = _db.SelectDetail(Config.Tables[(int)Config.eTName._credit], CreditStr);
            dvCreditInfo.DataSource = Config.creditds.Tables[0];
            Config.CardName = new string[Config.creditds.Tables[0].Rows.Count];
            foreach (DataRow r in Config.creditds.Tables[0].Rows)
            {
                Config.CardName[cnt] = r["카드명"].ToString();
                lblCreditTerm.Text = r["사용실적기간"].ToString() + " | 출금일 " + r["출금일"].ToString();
                string str = Regex.Replace(r["사용실적기간"].ToString(), @"\D", "");
                Config.CreditTerm[0] = Convert.ToInt32(str.Substring(0, 2));
                Config.CreditTerm[1] = Convert.ToInt32(str.Substring(2, 2));
                str = Regex.Replace(r["출금일"].ToString(), @"\D", "");
                Config.CreditDate = Convert.ToInt32(str);
                cnt++;
            }
        }

        public void InitAccountBook()
        {
            cbUseType.Items.Clear();
            cbUseType.Items.AddRange(Config.UseType);

            //가계부 상세내역 DB에서 읽어와서 DataSet에 저장하고 dvAccountBook DataGridView에 DataSource 바인딩하기
            string BookStr = "no as 'No.', use_date as '날짜', use_type as '구분', account as '계정', ie_type as '입출금구분', " +
                    "ie_standard as '입출금기준', use_cost as '금액', remarks as '상세내역'";
            Config.books = _db.SelectDetail(Config.Tables[(int)Config.eTName._book], BookStr);
            dvAccountBook.DataSource = Config.books.Tables[0];
            bookcnt = Config.books.Tables[0].Rows.Count;

            //dvAccountBook DataGridView 각 컬럼별 속성 지정하기
            dvAccountBook.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dvAccountBook.Columns[0].Width = 40;
            dvAccountBook.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvAccountBook.Columns[1].DefaultCellStyle.Format = "yyyy-MM-dd";
            dvAccountBook.Columns[6].DefaultCellStyle.Format = "###,##0";
            dvAccountBook.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvAccountBook.Sort(dvAccountBook.Columns[0], ListSortDirection.Descending);

            if (Config.creditds.Tables.Count > 0)
            {
                //신용카드 사용실적기간 확인 후 신용카드 전체 사용금액 합산
                //사용실적기간은 ''전월 00일 ~ 당월 00일'' 으로 적어줘야함
                DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, Config.CreditTerm[0]);
                if (DateTime.Compare(DateTime.Now.Date, dt.Date) < 0)
                {
                    DateTime bdt = dt.AddMonths(-1);
                    string str3 = $"sum(case ie_type when '신용카드' then +use_cost end) as c";
                    DataSet ds3 = _db.SelectDetail(Config.Tables[(int)Config.eTName._book], str3,
                        $"where use_date between '{bdt.Year.ToString()}-{(bdt.Month).ToString()}-{Config.CreditTerm[0].ToString()}' " +
                        $"and '{today.Year.ToString()}-{(today.Month).ToString()}-{Config.CreditTerm[1].ToString()}'");
                    if (ds3.Tables[0].Rows[0]["c"] == DBNull.Value) ds3.Tables[0].Rows[0]["c"] = 0;
                    iCreditAuto = Convert.ToInt32(ds3.Tables[0].Rows[0]["c"]);
                    txtCreditAuto.Text = iCreditAuto.ToString("###,##0");
                }
                else
                {
                    string str3 = $"sum(case ie_type when '신용카드' then +use_cost end) as c";
                    DataSet ds3 = _db.SelectDetail(Config.Tables[(int)Config.eTName._book], str3,
                        $"where use_date between '{today.Year.ToString()}-{(today.Month).ToString()}-{Config.CreditTerm[1].ToString()}'" +
                        $"and '{today.ToString("yyyy-MM-dd")}'");
                    if (ds3.Tables[0].Rows[0]["c"] == DBNull.Value) ds3.Tables[0].Rows[0]["c"] = 0;
                    iCreditAuto = Convert.ToInt32(ds3.Tables[0].Rows[0]["c"]);
                    txtCreditAuto.Text = iCreditAuto.ToString("###,##0");
                }
            }
        }

        public void InitMain()
        {
            //각 변수 초기화
            iTotalImport = 0;
            iTotalExport = 0;
            iTotalMImport = 0;
            iTotalMExport = 0;
            iTotalAccount = 0;

            //각 계정별, 계좌별 금액 계산을 위한 배열
            int[] importCostM = new int[Config.ImportInfo.Length];
            int[] exportCostM = new int[Config.ExportInfo.Length];
            int[] accountCost = new int[Config.AccountName.Length];

            foreach (DataRow r in Config.books.Tables[0].Rows) //가계부 내역 전체
            {
                //DataSet에 들어가 있는 C# Date 형식은 ("1/3/2020 12:00:00 AM") 처럼 현재 Window 형식을 따라가기 때문에
                //다음과 같이 DB Date 형식 ("2020-03-01") 으로 비교하기 편하게 변경해줍니다.
                DateTime dt = DateTime.Parse(r["날짜"].ToString());
                string date = dt.ToString("yyyy-MM-dd");

                if (r["구분"].ToString() == "수입") //수입의 경우
                {
                    iTotalImport += Convert.ToInt32(r["금액"]); //전체 수입 금액 SUM
                    if (date.Contains($"{today.Year}-{today.Month.ToString("00")}-")) //당월 수입 내역인지?
                    {
                        for (int j = 0; j < Config.ImportInfo.Length; j++)
                        {
                            if (r["계정"].ToString() == Config.ImportInfo[j])
                            {
                                importCostM[j] += Convert.ToInt32(r["금액"].ToString()); //계정별 수입 금액
                                iTotalMImport += Convert.ToInt32(r["금액"].ToString()); //당월 총 수입 금액
                            }
                        }
                    }
                }
                else if (r["구분"].ToString() == "지출") //지출의 경우
                {
                    iTotalExport += Convert.ToInt32(r["금액"]); //전체 지출 금액 SUM
                    for (int j = 0; j < Config.ExportInfo.Length; j++)
                    {
                        if (date.Contains($"{today.Year}-{today.Month.ToString("00")}-")) //당월 지출 내역인지?
                        {
                            if (r["계정"].ToString() == Config.ExportInfo[j])
                            {
                                exportCostM[j] += Convert.ToInt32(r["금액"].ToString()); //계정별 지출 금액
                                iTotalMExport += Convert.ToInt32(r["금액"].ToString()); //당월 총 지출 금액
                            }
                        }
                    }
                }
                iTotalAccount = iTotalImport - iTotalExport; //총 금액 = 총 수입 내역 - 총 지출 내역
            }
            //화면에 보여질 때의 String Format 지정
            txtTotalAccount.Text = iTotalAccount.ToString("###,##0");
            txtTotalImport.Text = iTotalImport.ToString("###,##0");
            txtTotalExport.Text = iTotalExport.ToString("###,##0");
            txtMImport.Text = iTotalMImport.ToString("###,##0");
            txtMExport.Text = iTotalMExport.ToString("###,##0");

            foreach (DataRow r in Config.books.Tables[0].Rows) //가계부 내역 전체
            {
                for (int i = 0; i < Config.AccountName.Length; i++) //계좌별 잔액 SUM
                {
                    //계좌이동의 경우 돈이 입출금구분 계좌 -> 입출금기준 계좌 로 이동 
                    if (r["입출금구분"].ToString() == Config.AccountName[i])
                    {
                        if (r["구분"].ToString() == "계좌이동") accountCost[i] -= Convert.ToInt32(r["금액"].ToString());
                        else if (r["구분"].ToString() == "신카출금") accountCost[i] -= Convert.ToInt32(r["금액"].ToString());
                    }
                    else if (r["입출금기준"].ToString() == Config.AccountName[i] || r["입출금기준"].ToString() == Config.ConnectCard[i])
                    {
                        if (r["구분"].ToString() == "수입") accountCost[i] += Convert.ToInt32(r["금액"].ToString());
                        else if (r["구분"].ToString() == "지출") accountCost[i] -= Convert.ToInt32(r["금액"].ToString());
                        else if (r["구분"].ToString() == "계좌이동") accountCost[i] += Convert.ToInt32(r["금액"].ToString());
                        else if (r["구분"].ToString() == "신카출금") accountCost[i] -= Convert.ToInt32(r["금액"].ToString());
                    }
                }
            }
            iTotalAccount = 0;
            for (int i = 0; i < accountCost.Length; i++) //계좌별 최초잔액 SUM
            {
                accountCost[i] += Convert.ToInt32(Config.accountds.Tables[0].Rows[i]["최초잔액"]);
                iTotalAccount += accountCost[i];
            }
            txtTotal.Text = iTotalAccount.ToString("###,##0");

            //당월 계정별 수입 테이블 생성 (컬럼 2개 추가)
            Config.importdt = new DataTable("importdt");
            Config.importdt.Columns.Add("계정");
            Config.importdt.Columns.Add("금액");
            Config.importdt.Columns[1].DataType = typeof(Int32);
            DataRow imdr;
            for (int i = 0; i < Config.ImportInfo.Length; i++)
            {
                imdr = Config.importdt.NewRow(); //NewRow 사용
                imdr["계정"] = Config.ImportInfo[i]; //수입 계정 정보 추가
                imdr["금액"] = importCostM[i]; //위에서 SUM 했던 계정별 수입 금액
                Config.importdt.Rows.Add(imdr);
            }
            dvImportList.DataSource = Config.importdt; //셍성한 테이블 DataSource 바인딩
            dvImportList.Columns[1].DefaultCellStyle.Format = "###,##0";
            dvImportList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //당월 계정별 지출 테이블 생성 (컬럼 2개 추가)
            Config.exportdt = new DataTable("exportdt");
            Config.exportdt.Columns.Add("계정");
            Config.exportdt.Columns.Add("금액");
            Config.exportdt.Columns[1].DataType = typeof(Int32);
            DataRow exdr;
            for (int i = 0; i < Config.ExportInfo.Length; i++)
            {
                exdr = Config.exportdt.NewRow(); //NewRow 사용
                exdr["계정"] = Config.ExportInfo[i]; //지출 계정 정보 추가
                exdr["금액"] = exportCostM[i]; //위에서 SUM 했던 계정별 지출 금액
                Config.exportdt.Rows.Add(exdr);
            }
            dvExportList.DataSource = Config.exportdt; //생성한 테이블 DataSource 바인딩
            dvExportList.Columns[1].DefaultCellStyle.Format = "###,##0";
            dvExportList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //전체 계좌별 잔액 조회 테이블 생성 (컬럼 3개 추가)
            Config.totaldt = new DataTable("exportdt");
            Config.totaldt.Columns.Add("계좌");
            Config.totaldt.Columns.Add("은행");
            Config.totaldt.Columns.Add("금액");
            Config.totaldt.Columns[2].DataType = typeof(Int32);
            DataRow accountdr;
            for (int i = 0; i < Config.AccountName.Length; i++)
            {
                accountdr = Config.totaldt.NewRow(); //NewRow 사용
                accountdr["계좌"] = Config.AccountName[i]; //계좌 정보 추가 
                accountdr["은행"] = Config.accountds.Tables[0].Rows[i]["은행명"]; //계좌별 은행 이름 추가
                accountdr["금액"] = accountCost[i]; //위에서 SUM 했던 계좌별 금액
                Config.totaldt.Rows.Add(accountdr);
            }
            dvAccountList.DataSource = Config.totaldt; //생성한 테이블 DataSource 바인딩
            dvAccountList.Columns[2].DefaultCellStyle.Format = "###,##0";
            dvAccountList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
        
        private void cbUseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbAccount.Text = "";
            cbIEType.Text = "";
            cbIEStandard.Text = "";
            cbAccount.Items.Clear();
            cbIEType.Items.Clear();

            if (cbUseType.SelectedIndex == 0) //수입선택
            {
                cbAccount.Enabled = true;
                cbAccount.Items.AddRange(Config.ImportInfo);
                cbIEType.Items.AddRange(Config.AccountI);
            }
            else if (cbUseType.SelectedIndex == 1)  //지출선택
            {
                cbAccount.Enabled = true;
                cbAccount.Items.AddRange(Config.ExportInfo);
                cbIEType.Items.AddRange(Config.AccountE);
            }
            else if (cbUseType.SelectedIndex == 2) //계좌이동 선택
            {
                cbAccount.Enabled = false;
                cbIEType.Items.AddRange(Config.AccountName);
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.AccountName);
            }
            else if (cbUseType.SelectedIndex == 3) //신카출금선택
            {
                cbAccount.Enabled = false;
                cbIEType.Items.AddRange(Config.AccountName);
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.AccountName);
                txtRemarks.Text = "신카출금";
            }
        }

        private void dvAccountBook_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //BookUpdate 객체 생성 후 선택한 행의 값 전달해주기
            _bookUpdate = new fBookUpdate(this);
            int rowIndex = dvAccountBook.CurrentRow.Index; //클릭한 위치의 rowIndex 받아오기
            _bookUpdate.lblNumber.Text = dvAccountBook.Rows[rowIndex].Cells[0].Value.ToString();
            _bookUpdate.txtUseDate.Text = dvAccountBook.Rows[rowIndex].Cells[1].Value.ToString();
            _bookUpdate.cbUseType.Text = dvAccountBook.Rows[rowIndex].Cells[2].Value.ToString();
            _bookUpdate.cbAccount.Text = dvAccountBook.Rows[rowIndex].Cells[3].Value.ToString();
            _bookUpdate.cbIEType.Text = dvAccountBook.Rows[rowIndex].Cells[4].Value.ToString();
            _bookUpdate.cbIEStandard.Text = dvAccountBook.Rows[rowIndex].Cells[5].Value.ToString();
            _bookUpdate.txtUseCost.Text = dvAccountBook.Rows[rowIndex].Cells[6].Value.ToString();
            _bookUpdate.txtRemarks.Text = dvAccountBook.Rows[rowIndex].Cells[7].Value.ToString();
            _bookUpdate.Show();
        }

        public void ChangeApply()
        {
            //값 업데이트 혹은 삭제 시 변경사항 적용
            InitAccountBook();
            InitMain();
            dvAccountBook.Refresh();
            dvAccountList.Refresh();
            dvImportList.Refresh();
            dvExportList.Refresh();
            Application.DoEvents();  
        }

        public void updateBook(int num, string date, string useType, string account, string IEtype, string IEstandard, int useCost, string remarks)
        {
            //가계부 상세내역 업데이트하기
            string value = $"use_date='{date}', use_type='{useType}', account='{account}', ie_type='{IEtype}', ie_standard='{IEstandard}', use_cost='{useCost}', remarks='{remarks}'";
            _db.Update(Config.Tables[(int)Config.eTName._book], value, $"no='{num.ToString()}'");
        }

        private void dvSettingInfo_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            int col = e.ColumnIndex;

            //수입계정, 지출계정 값 변경 시 마다 데이터베이스에 입력 or 수정하기
            if (Config.settingds.Tables[0].Rows.Count == row)
            {
                //새로운 행에 작성 -> Insert
                if (col == 0) _db.Insert(Config.Tables[(int)Config.eTName._setting] + "(import_info)",
                    $"'{dvSettingInfo.Rows[row].Cells[col].Value.ToString()}'");
                else _db.Insert(Config.Tables[(int)Config.eTName._setting] + "(export_info)",
                    $"'{dvSettingInfo.Rows[row].Cells[col].Value.ToString()}'");
            }
            else
            {
                //기존에 있던 행에 수정 및 작성 -> Update
                if (col == 0) _db.Update(Config.Tables[(int)Config.eTName._setting],
                    $"import_info='{dvSettingInfo.Rows[row].Cells[col].Value.ToString()}'",
                    $"(@rownum:=0)=0 and (@rownum:=@rownum+1)={row + 1}");
                else _db.Update(Config.Tables[(int)Config.eTName._setting],
                    $"export_info='{dvSettingInfo.Rows[row].Cells[col].Value.ToString()}'",
                    $"(@rownum:=0)=0 and (@rownum:=@rownum+1)={row + 1}");
            }
            //settingds 데이터셋 업데이트
            Config.settingds = _db.SelectDetail(Config.Tables[(int)Config.eTName._setting], 
                "import_info as '수입계정', export_info as '지출계정'");

            ChangeApply();
        }

        public void deleteBook(string num)
        {
            //가계부 상세 내역 삭제
            _db.DeleteDetail(Config.Tables[(int)Config.eTName._book], "no", num);
        }
        
        private void cbIEType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbIEStandard.Text = "";

            if (cbIEType.Text == "현금")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.Cash);
            }
            else if (cbIEType.Text == "체크카드")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.ConnectCard);
            }
            else if (cbIEType.Text == "신용카드")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.CardName);
            }
            else if (cbIEType.Text == "계좌입금" || cbIEType.Text == "타인계좌이체")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.AccountName);
            }
        }

        private void btnSettingSave_Click(object sender, EventArgs e)
        {
            _db.DeleteAll(Config.Tables[(int)Config.eTName._account]);
            for (int i = 0; i < dvAccountInfo.RowCount - 1; i++)
            {
                _db.Insert(Config.Tables[(int)Config.eTName._account], $"'{dvAccountInfo.Rows[i].Cells[0].Value.ToString()}'," +
                    $"'{dvAccountInfo.Rows[i].Cells[1].Value.ToString()}', '{dvAccountInfo.Rows[i].Cells[2].Value.ToString()}'," +
                    $"'{dvAccountInfo.Rows[i].Cells[3].Value.ToString()}', '{dvAccountInfo.Rows[i].Cells[4].Value.ToString()}'");
            }
            _db.DeleteAll(Config.Tables[(int)Config.eTName._credit]);
            for (int i = 0; i < dvCreditInfo.RowCount - 1; i++)
            {
                _db.Insert(Config.Tables[(int)Config.eTName._credit], $"'{dvCreditInfo.Rows[i].Cells[0].Value.ToString()}'," +
                    $"'{dvCreditInfo.Rows[i].Cells[1].Value.ToString()}', '{dvCreditInfo.Rows[i].Cells[2].Value.ToString()}'," +
                    $"'{dvCreditInfo.Rows[i].Cells[3].Value.ToString()}', '{dvCreditInfo.Rows[i].Cells[4].Value.ToString()}'," +
                    $"'{dvCreditInfo.Rows[i].Cells[5].Value.ToString()}'");
            }
            ChangeApply();
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            //가계부 상세 내역 새로운 값 입력
            if (txtUseCost.Text == "") txtUseCost.Text = "0";

            string value = $"{bookcnt + 1}, '{txtUseDate.Text}', '{cbUseType.Text}', '{cbAccount.Text}', " + 
                "'{cbIEType.Text}', '{cbIEStandard.Text}', { Convert.ToInt32(txtUseCost.Text)},'{txtRemarks.Text}'";
            _db.Insert(Config.Tables[(int)Config.eTName._book], value);

            cbUseType.Text = "";
            cbAccount.Enabled = true;
            cbAccount.Text = "";
            cbIEType.Text = "";
            cbIEStandard.Text = "";
            txtUseCost.Text = "";
            txtRemarks.Text = "";

            ChangeApply();
        }
    }
}
