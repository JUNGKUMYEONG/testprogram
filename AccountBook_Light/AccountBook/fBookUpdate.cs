﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccountBook
{
    public partial class fBookUpdate : Form
    {
        fMain _main = new fMain();

        public fBookUpdate()
        {
            InitializeComponent();
            cbUseType.Items.AddRange(Config.UseType);
        }
        public fBookUpdate(fMain form)
        {
            InitializeComponent();
            _main = form;
        }

        private void cbUseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbAccount.Text = "";
            cbIEType.Text = "";
            cbIEStandard.Text = "";
            cbAccount.Items.Clear();
            cbIEType.Items.Clear();

            if (cbUseType.SelectedIndex == 0)
            {
                cbAccount.Enabled = true;
                cbAccount.Items.AddRange(Config.ImportInfo);
                cbIEType.Items.AddRange(Config.AccountI);
            }
            else if (cbUseType.SelectedIndex == 1)
            {
                cbAccount.Enabled = true;
                cbAccount.Items.AddRange(Config.ExportInfo);
                cbIEType.Items.AddRange(Config.AccountE);
            }
            else if (cbUseType.SelectedIndex == 2)
            {
                cbAccount.Enabled = false;
                cbIEType.Items.AddRange(Config.AccountName);
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.AccountName);
            }
            else if (cbUseType.SelectedIndex == 3)
            {
                cbAccount.Enabled = false;
                cbIEType.Items.AddRange(Config.AccountName);
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.AccountName);
            }
        }

        private void cbIEType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbIEStandard.Text = "";

            if (cbIEType.Text == "현금")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.Cash);
            }
            else if (cbIEType.Text == "체크카드")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.ConnectCard);
            }
            else if (cbIEType.Text == "신용카드")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.CardName);
            }
            else if (cbIEType.Text == "계좌입금" || cbIEType.Text == "타인계좌이체")
            {
                cbIEStandard.Items.Clear();
                cbIEStandard.Items.AddRange(Config.AccountName);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtUseCost.Text == "") txtUseCost.Text = "0";

            _main.updateBook(Convert.ToInt32(lblNumber.Text), txtUseDate.Text, cbUseType.Text, cbAccount.Text,
                cbIEType.Text, cbIEStandard.Text, Convert.ToInt32(txtUseCost.Text), txtRemarks.Text);

            cbUseType.Text = "";
            cbAccount.Enabled = true;
            cbAccount.Text = "";
            cbIEType.Text = "";
            cbIEStandard.Text = "";
            txtUseCost.Text = "";
            txtRemarks.Text = "";

            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            cbUseType.Text = "";
            cbAccount.Enabled = true;
            cbAccount.Text = "";
            cbIEType.Text = "";
            cbIEStandard.Text = "";
            txtUseCost.Text = "";
            txtRemarks.Text = "";

            Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _main.deleteBook(lblNumber.Text);

            cbUseType.Text = "";
            cbAccount.Enabled = true;
            cbAccount.Text = "";
            cbIEType.Text = "";
            cbIEStandard.Text = "";
            txtUseCost.Text = "";
            txtRemarks.Text = "";

            Close();
        }

        private void fBookUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            _main.ChangeApply();
        }
    }
}
