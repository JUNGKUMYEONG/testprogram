﻿namespace AccountBook
{
    partial class fBookUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fBookUpdate));
            this.txtUseDate = new System.Windows.Forms.DateTimePicker();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtUseCost = new System.Windows.Forms.TextBox();
            this.cbIEStandard = new System.Windows.Forms.ComboBox();
            this.cbIEType = new System.Windows.Forms.ComboBox();
            this.cbAccount = new System.Windows.Forms.ComboBox();
            this.cbUseType = new System.Windows.Forms.ComboBox();
            this.lblNumber = new System.Windows.Forms.Label();
            this.dvEdit = new System.Windows.Forms.DataGridView();
            this.txtUseDate1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbUseType1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbAccount1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbIEType1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbIEStandard1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.txtCost1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRemarks1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSet1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dvEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUseDate
            // 
            this.txtUseDate.CustomFormat = "yyyy-MM-dd";
            this.txtUseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtUseDate.Location = new System.Drawing.Point(16, 60);
            this.txtUseDate.Name = "txtUseDate";
            this.txtUseDate.Size = new System.Drawing.Size(148, 22);
            this.txtUseDate.TabIndex = 10;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(1054, 60);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(147, 22);
            this.btnUpdate.TabIndex = 17;
            this.btnUpdate.Text = "수정 완료";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(906, 60);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(148, 22);
            this.txtRemarks.TabIndex = 16;
            // 
            // txtUseCost
            // 
            this.txtUseCost.Location = new System.Drawing.Point(758, 60);
            this.txtUseCost.Name = "txtUseCost";
            this.txtUseCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtUseCost.Size = new System.Drawing.Size(148, 22);
            this.txtUseCost.TabIndex = 15;
            // 
            // cbIEStandard
            // 
            this.cbIEStandard.FormattingEnabled = true;
            this.cbIEStandard.Location = new System.Drawing.Point(609, 60);
            this.cbIEStandard.Name = "cbIEStandard";
            this.cbIEStandard.Size = new System.Drawing.Size(149, 22);
            this.cbIEStandard.TabIndex = 14;
            // 
            // cbIEType
            // 
            this.cbIEType.FormattingEnabled = true;
            this.cbIEType.Location = new System.Drawing.Point(461, 60);
            this.cbIEType.Name = "cbIEType";
            this.cbIEType.Size = new System.Drawing.Size(148, 22);
            this.cbIEType.TabIndex = 13;
            this.cbIEType.SelectedIndexChanged += new System.EventHandler(this.cbIEType_SelectedIndexChanged);
            // 
            // cbAccount
            // 
            this.cbAccount.FormattingEnabled = true;
            this.cbAccount.Location = new System.Drawing.Point(312, 60);
            this.cbAccount.Name = "cbAccount";
            this.cbAccount.Size = new System.Drawing.Size(149, 22);
            this.cbAccount.TabIndex = 12;
            // 
            // cbUseType
            // 
            this.cbUseType.FormattingEnabled = true;
            this.cbUseType.Location = new System.Drawing.Point(164, 60);
            this.cbUseType.Name = "cbUseType";
            this.cbUseType.Size = new System.Drawing.Size(148, 22);
            this.cbUseType.TabIndex = 11;
            this.cbUseType.SelectedIndexChanged += new System.EventHandler(this.cbUseType_SelectedIndexChanged);
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.Location = new System.Drawing.Point(12, 10);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(56, 18);
            this.lblNumber.TabIndex = 19;
            this.lblNumber.Text = "Number";
            // 
            // dvEdit
            // 
            this.dvEdit.AllowUserToAddRows = false;
            this.dvEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvEdit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvEdit.BackgroundColor = System.Drawing.Color.LightGray;
            this.dvEdit.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dvEdit.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dvEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvEdit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtUseDate1,
            this.cbUseType1,
            this.cbAccount1,
            this.cbIEType1,
            this.cbIEStandard1,
            this.txtCost1,
            this.txtRemarks1,
            this.btnSet1});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvEdit.DefaultCellStyle = dataGridViewCellStyle4;
            this.dvEdit.Enabled = false;
            this.dvEdit.EnableHeadersVisualStyles = false;
            this.dvEdit.Location = new System.Drawing.Point(15, 35);
            this.dvEdit.Name = "dvEdit";
            this.dvEdit.RowHeadersVisible = false;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dvEdit.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dvEdit.RowTemplate.Height = 23;
            this.dvEdit.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dvEdit.Size = new System.Drawing.Size(1188, 49);
            this.dvEdit.TabIndex = 18;
            // 
            // txtUseDate1
            // 
            dataGridViewCellStyle1.Format = "M";
            dataGridViewCellStyle1.NullValue = null;
            this.txtUseDate1.DefaultCellStyle = dataGridViewCellStyle1;
            this.txtUseDate1.HeaderText = "날짜";
            this.txtUseDate1.Name = "txtUseDate1";
            // 
            // cbUseType1
            // 
            this.cbUseType1.HeaderText = "구분";
            this.cbUseType1.Items.AddRange(new object[] {
            "수입",
            "지출",
            "계좌이동"});
            this.cbUseType1.Name = "cbUseType1";
            this.cbUseType1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbUseType1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbAccount1
            // 
            this.cbAccount1.HeaderText = "계정";
            this.cbAccount1.Name = "cbAccount1";
            this.cbAccount1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbAccount1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbIEType1
            // 
            this.cbIEType1.HeaderText = "입/출금 구분";
            this.cbIEType1.Name = "cbIEType1";
            this.cbIEType1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbIEType1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbIEStandard1
            // 
            this.cbIEStandard1.HeaderText = "입/출금 기준";
            this.cbIEStandard1.Name = "cbIEStandard1";
            this.cbIEStandard1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbIEStandard1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txtCost1
            // 
            dataGridViewCellStyle2.Format = "C0";
            dataGridViewCellStyle2.NullValue = null;
            this.txtCost1.DefaultCellStyle = dataGridViewCellStyle2;
            this.txtCost1.HeaderText = "금액";
            this.txtCost1.Name = "txtCost1";
            // 
            // txtRemarks1
            // 
            this.txtRemarks1.HeaderText = "상세 내역 및 비고";
            this.txtRemarks1.Name = "txtRemarks1";
            // 
            // btnSet1
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.NullValue = "입력";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.btnSet1.DefaultCellStyle = dataGridViewCellStyle3;
            this.btnSet1.HeaderText = "입력";
            this.btnSet1.Name = "btnSet1";
            this.btnSet1.ReadOnly = true;
            this.btnSet1.Text = "입력";
            this.btnSet1.UseColumnTextForButtonValue = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(100, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 18);
            this.label1.TabIndex = 20;
            this.label1.Text = "수정내역 입력";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(1133, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 22);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "취소";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(1054, 7);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 22);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "삭제";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // fBookUpdate
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1214, 96);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUseDate);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.txtUseCost);
            this.Controls.Add(this.cbIEStandard);
            this.Controls.Add(this.cbIEType);
            this.Controls.Add(this.cbAccount);
            this.Controls.Add(this.cbUseType);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.dvEdit);
            this.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fBookUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Account Book";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fBookUpdate_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dvEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView dvEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtUseDate1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbUseType1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbAccount1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbIEType1;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbIEStandard1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCost1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtRemarks1;
        private System.Windows.Forms.DataGridViewButtonColumn btnSet1;
        public System.Windows.Forms.DateTimePicker txtUseDate;
        public System.Windows.Forms.TextBox txtRemarks;
        public System.Windows.Forms.TextBox txtUseCost;
        public System.Windows.Forms.ComboBox cbIEStandard;
        public System.Windows.Forms.ComboBox cbIEType;
        public System.Windows.Forms.ComboBox cbAccount;
        public System.Windows.Forms.ComboBox cbUseType;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
    }
}