﻿namespace FlottingGrid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pbZoom = new System.Windows.Forms.PictureBox();
            this.label43 = new System.Windows.Forms.Label();
            this.pbRuler1 = new System.Windows.Forms.PictureBox();
            this.pbRuler2 = new System.Windows.Forms.PictureBox();
            this.pnlZoom = new System.Windows.Forms.Panel();
            this.lblPosition = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRuler1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRuler2)).BeginInit();
            this.pnlZoom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbZoom
            // 
            this.pbZoom.BackColor = System.Drawing.Color.Black;
            this.pbZoom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbZoom.Image = ((System.Drawing.Image)(resources.GetObject("pbZoom.Image")));
            this.pbZoom.Location = new System.Drawing.Point(29, 30);
            this.pbZoom.Name = "pbZoom";
            this.pbZoom.Size = new System.Drawing.Size(590, 590);
            this.pbZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbZoom.TabIndex = 1063;
            this.pbZoom.TabStop = false;
            this.pbZoom.Paint += new System.Windows.Forms.PaintEventHandler(this.pbZoom_Paint);
            this.pbZoom.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbZoom_MouseDown);
            this.pbZoom.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbZoom_MouseMove);
            this.pbZoom.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbZoom_MouseUp);
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.Color.White;
            this.label43.Font = new System.Drawing.Font("Malgun Gothic", 20F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label43.Location = new System.Drawing.Point(668, 674);
            this.label43.Margin = new System.Windows.Forms.Padding(3);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(46, 40);
            this.label43.TabIndex = 1075;
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbRuler1
            // 
            this.pbRuler1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbRuler1.BackgroundImage")));
            this.pbRuler1.Location = new System.Drawing.Point(29, 0);
            this.pbRuler1.Name = "pbRuler1";
            this.pbRuler1.Size = new System.Drawing.Size(591, 30);
            this.pbRuler1.TabIndex = 1109;
            this.pbRuler1.TabStop = false;
            // 
            // pbRuler2
            // 
            this.pbRuler2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbRuler2.BackgroundImage")));
            this.pbRuler2.Location = new System.Drawing.Point(-1, 30);
            this.pbRuler2.Name = "pbRuler2";
            this.pbRuler2.Size = new System.Drawing.Size(30, 590);
            this.pbRuler2.TabIndex = 1110;
            this.pbRuler2.TabStop = false;
            // 
            // pnlZoom
            // 
            this.pnlZoom.BackColor = System.Drawing.Color.Transparent;
            this.pnlZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlZoom.Controls.Add(this.pbZoom);
            this.pnlZoom.Controls.Add(this.pbRuler2);
            this.pnlZoom.Controls.Add(this.pbRuler1);
            this.pnlZoom.Controls.Add(this.label43);
            this.pnlZoom.Controls.Add(this.label2);
            this.pnlZoom.Location = new System.Drawing.Point(0, 0);
            this.pnlZoom.Name = "pnlZoom";
            this.pnlZoom.Size = new System.Drawing.Size(621, 622);
            this.pnlZoom.TabIndex = 1064;
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(5, 629);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(65, 12);
            this.lblPosition.TabIndex = 1066;
            this.lblPosition.Text = "X : 0, Y : 0";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Cornsilk;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 20F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label2.Location = new System.Drawing.Point(-1, -1);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(233, 40);
            this.label2.TabIndex = 1111;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 644);
            this.Controls.Add(this.lblPosition);
            this.Controls.Add(this.pnlZoom);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pbZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRuler1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRuler2)).EndInit();
            this.pnlZoom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbZoom;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.PictureBox pbRuler1;
        private System.Windows.Forms.PictureBox pbRuler2;
        private System.Windows.Forms.Panel pnlZoom;
        private System.Windows.Forms.Label lblPosition;
        private System.Windows.Forms.Label label2;
    }
}

