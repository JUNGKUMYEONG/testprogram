﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlottingGrid
{
    public partial class Form1 : Form
    {
        private float zoomRatio = 1.0f;
        private double recRatio = 3.6875;
        private Point imgPoint;
        private Rectangle imgRect;
        private Point clickPoint;

        public Form1()
        {
            InitializeComponent();

            pbZoom.MouseWheel += new MouseEventHandler(pbZoom_MouseWheel);
            imgPoint = new Point(pbZoom.Width / 2, pbZoom.Height / 2);
            imgRect = new Rectangle(0, 0, pbZoom.Width, pbZoom.Height);
            clickPoint = imgPoint;
            pbZoom.Invalidate();
        }

        private void pbZoom_Paint(object sender, PaintEventArgs e)
        {
            if (pbZoom.Image != null)
            {
                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                e.Graphics.DrawImage(pbZoom.Image, imgRect);
                pbZoom.Focus();
            }

            int Xn = pbZoom.Width;
            int Yn = pbZoom.Height;
            Pen pn = new Pen(Color.FromArgb(150, 144, 238, 144));
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            for (int y = -3; y < Yn; y += (38 * (int)zoomRatio))
            {
                e.Graphics.DrawLine(pn, 0, y, Xn, y);
            }
            for (int x = -3; x < Xn; x += (38 * (int)zoomRatio))
            {
                e.Graphics.DrawLine(pn, x, 0, x, Yn);
            }
        }

        private void pbZoom_MouseWheel(object sender, MouseEventArgs e)
        {
            int lines = e.Delta * SystemInformation.MouseWheelScrollLines / 120;
            PictureBox pb = (PictureBox)sender;

            if (lines > 0)
            {
                zoomRatio *= 1.1F;
                if (zoomRatio > 100.0) zoomRatio = 100.0f;
            }
            else if (lines < 0)
            {
                zoomRatio *= 0.9F;
                if (zoomRatio < 1) zoomRatio = 1;
            }

            imgRect.Width = (int)Math.Round(pbZoom.Width * zoomRatio);
            imgRect.Height = (int)Math.Round(pbZoom.Height * zoomRatio);
            imgRect.X = (int)Math.Round(pb.Width / 2 - imgPoint.X * zoomRatio);
            imgRect.Y = (int)Math.Round(pb.Height / 2 - imgPoint.Y * zoomRatio);

            pbZoom.Invalidate();
        }

        private void pbZoom_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                clickPoint = new Point(e.X, e.Y);
            }
            pbZoom.Invalidate();
        }

        private void pbZoom_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                lblPosition.Text = $"X : {e.X}, Y : {e.Y}";

                if(zoomRatio == 1)
                {
                    Pen pn = new Pen(Color.Black);
                    Graphics g = pbZoom.CreateGraphics();

                    float w = Math.Abs(clickPoint.X - e.X);
                    float h = Math.Abs(clickPoint.Y - e.Y);
                    if (e.X > clickPoint.X)
                    {
                        if (e.Y > clickPoint.Y) g.DrawRectangle(pn, clickPoint.X, clickPoint.Y, w, h);
                        else g.DrawRectangle(pn, clickPoint.X, e.Y, w, h);
                    }
                    else
                    {
                        if (e.Y > clickPoint.Y) g.DrawRectangle(pn, e.X, clickPoint.Y, w, h);
                        else g.DrawRectangle(pn, e.X, e.Y, w, h);
                    }
                }

                imgRect.X = imgRect.X + (int)Math.Round((double)(e.X - clickPoint.X) / 3);
                if (imgRect.X >= 0) imgRect.X = 0;
                if (Math.Abs(imgRect.X) >= Math.Abs(imgRect.Width - pbZoom.Width)) imgRect.X = -(imgRect.Width - pbZoom.Width);
                imgRect.Y = imgRect.Y + (int)Math.Round((double)(e.Y - clickPoint.Y) / 3);
                if (imgRect.Y >= 0) imgRect.Y = 0;
                if (Math.Abs(imgRect.Y) >= Math.Abs(imgRect.Height - pbZoom.Height)) imgRect.Y = -(imgRect.Height - pbZoom.Height);
                pbZoom.Invalidate();
            }
        }

        private void pbZoom_MouseUp(object sender, MouseEventArgs e)
        {
            if(zoomRatio == 1)
            {
                Pen pn = new Pen(Color.Black);
                Graphics g = pbZoom.CreateGraphics();

                float w = Math.Abs(clickPoint.X - e.X);
                float h = Math.Abs(clickPoint.Y - e.Y);
                if (e.X > clickPoint.X)
                {
                    if (e.Y > clickPoint.Y) g.DrawRectangle(pn, clickPoint.X, clickPoint.Y, w, h);
                    else g.DrawRectangle(pn, clickPoint.X, e.Y, w, h);
                }
                else
                {
                    if (e.Y > clickPoint.Y) g.DrawRectangle(pn, e.X, clickPoint.Y, w, h);
                    else g.DrawRectangle(pn, e.X, e.Y, w, h);
                }

                double distance = Math.Sqrt(Math.Pow((e.X - clickPoint.X), 2) + Math.Pow((e.Y - clickPoint.Y), 2));
                distance = distance / recRatio;
                lblPosition.Text = $"START=X:{clickPoint.X},Y:{clickPoint.Y} | END=X:{e.X},Y:{e.Y} | DISTANCE={distance.ToString(".####")}mm";
            }
        }
    }
}
