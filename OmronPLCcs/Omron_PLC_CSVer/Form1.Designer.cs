﻿namespace OmronPLCcs
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Active",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "IsConnected",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "PLC Type Name",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Connection Type",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Use Route Path",
            ""}, -1);
            this.label1 = new System.Windows.Forms.Label();
            this.tbIP = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnReadVariableMultiple = new System.Windows.Forms.Button();
            this.btnWriteVariable = new System.Windows.Forms.Button();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.btnReadVariable = new System.Windows.Forms.Button();
            this.txtVariableName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtReadRawMultiple = new System.Windows.Forms.Button();
            this.btnWriteRaw = new System.Windows.Forms.Button();
            this.txtBinaryValue = new System.Windows.Forms.TextBox();
            this.btnReadRaw = new System.Windows.Forms.Button();
            this.txtBinaryVariableName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnStatusReset = new System.Windows.Forms.Button();
            this.btnGetCommunication = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnTagTableSet = new System.Windows.Forms.Button();
            this.listViewOfVariableNames = new System.Windows.Forms.ListView();
            this.variableNameOfTagTable = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.variableTypeOfTagTable = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.variableValueOfTagTable = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Address";
            // 
            // tbIP
            // 
            this.tbIP.Location = new System.Drawing.Point(90, 24);
            this.tbIP.Name = "tbIP";
            this.tbIP.Size = new System.Drawing.Size(121, 22);
            this.tbIP.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDisconnect);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbPort);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbIP);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(7, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(218, 125);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PLC Connection";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(112, 86);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(100, 27);
            this.btnDisconnect.TabIndex = 2;
            this.btnDisconnect.Text = "DISCONNECT";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(8, 86);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(100, 27);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "CONNECT";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port";
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(90, 54);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(121, 22);
            this.tbPort.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnReadVariableMultiple);
            this.groupBox2.Controls.Add(this.btnWriteVariable);
            this.groupBox2.Controls.Add(this.txtValue);
            this.groupBox2.Controls.Add(this.btnReadVariable);
            this.groupBox2.Controls.Add(this.txtVariableName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(7, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(218, 155);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Variable Edit";
            // 
            // btnReadVariableMultiple
            // 
            this.btnReadVariableMultiple.Location = new System.Drawing.Point(8, 117);
            this.btnReadVariableMultiple.Name = "btnReadVariableMultiple";
            this.btnReadVariableMultiple.Size = new System.Drawing.Size(203, 27);
            this.btnReadVariableMultiple.TabIndex = 12;
            this.btnReadVariableMultiple.Text = "Read Variable Multiple";
            this.btnReadVariableMultiple.UseVisualStyleBackColor = true;
            this.btnReadVariableMultiple.Click += new System.EventHandler(this.btnReadVariableMultiple_Click);
            // 
            // btnWriteVariable
            // 
            this.btnWriteVariable.Location = new System.Drawing.Point(112, 83);
            this.btnWriteVariable.Name = "btnWriteVariable";
            this.btnWriteVariable.Size = new System.Drawing.Size(100, 27);
            this.btnWriteVariable.TabIndex = 11;
            this.btnWriteVariable.Text = "Write";
            this.btnWriteVariable.UseVisualStyleBackColor = true;
            this.btnWriteVariable.Click += new System.EventHandler(this.btnWriteVariable_Click);
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(79, 51);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(132, 22);
            this.txtValue.TabIndex = 9;
            // 
            // btnReadVariable
            // 
            this.btnReadVariable.Location = new System.Drawing.Point(8, 83);
            this.btnReadVariable.Name = "btnReadVariable";
            this.btnReadVariable.Size = new System.Drawing.Size(100, 27);
            this.btnReadVariable.TabIndex = 10;
            this.btnReadVariable.Text = "Read";
            this.btnReadVariable.UseVisualStyleBackColor = true;
            this.btnReadVariable.Click += new System.EventHandler(this.btnReadVariable_Click);
            // 
            // txtVariableName
            // 
            this.txtVariableName.Location = new System.Drawing.Point(79, 21);
            this.txtVariableName.Name = "txtVariableName";
            this.txtVariableName.Size = new System.Drawing.Size(132, 22);
            this.txtVariableName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "Value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtReadRawMultiple);
            this.groupBox3.Controls.Add(this.btnWriteRaw);
            this.groupBox3.Controls.Add(this.txtBinaryValue);
            this.groupBox3.Controls.Add(this.btnReadRaw);
            this.groupBox3.Controls.Add(this.txtBinaryVariableName);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(7, 174);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(218, 155);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Binary Variable Edit";
            // 
            // txtReadRawMultiple
            // 
            this.txtReadRawMultiple.Location = new System.Drawing.Point(8, 117);
            this.txtReadRawMultiple.Name = "txtReadRawMultiple";
            this.txtReadRawMultiple.Size = new System.Drawing.Size(203, 27);
            this.txtReadRawMultiple.TabIndex = 12;
            this.txtReadRawMultiple.Text = "Read Raw Multiple";
            this.txtReadRawMultiple.UseVisualStyleBackColor = true;
            this.txtReadRawMultiple.Click += new System.EventHandler(this.txtReadRawMultiple_Click);
            // 
            // btnWriteRaw
            // 
            this.btnWriteRaw.Location = new System.Drawing.Point(112, 83);
            this.btnWriteRaw.Name = "btnWriteRaw";
            this.btnWriteRaw.Size = new System.Drawing.Size(100, 27);
            this.btnWriteRaw.TabIndex = 11;
            this.btnWriteRaw.Text = "Write Raw";
            this.btnWriteRaw.UseVisualStyleBackColor = true;
            this.btnWriteRaw.Click += new System.EventHandler(this.btnWriteRaw_Click);
            // 
            // txtBinaryValue
            // 
            this.txtBinaryValue.Location = new System.Drawing.Point(79, 51);
            this.txtBinaryValue.Name = "txtBinaryValue";
            this.txtBinaryValue.Size = new System.Drawing.Size(132, 22);
            this.txtBinaryValue.TabIndex = 9;
            // 
            // btnReadRaw
            // 
            this.btnReadRaw.Location = new System.Drawing.Point(8, 83);
            this.btnReadRaw.Name = "btnReadRaw";
            this.btnReadRaw.Size = new System.Drawing.Size(100, 27);
            this.btnReadRaw.TabIndex = 10;
            this.btnReadRaw.Text = "Read Raw";
            this.btnReadRaw.UseVisualStyleBackColor = true;
            this.btnReadRaw.Click += new System.EventHandler(this.btnReadRaw_Click);
            // 
            // txtBinaryVariableName
            // 
            this.txtBinaryVariableName.Location = new System.Drawing.Point(79, 21);
            this.txtBinaryVariableName.Name = "txtBinaryVariableName";
            this.txtBinaryVariableName.Size = new System.Drawing.Size(132, 22);
            this.txtBinaryVariableName.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 8;
            this.label5.Text = "Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "Name";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listView1.HideSelection = false;
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.listView1.Location = new System.Drawing.Point(7, 23);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(304, 94);
            this.listView1.TabIndex = 14;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 180;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnStatusReset);
            this.groupBox4.Controls.Add(this.btnGetCommunication);
            this.groupBox4.Controls.Add(this.listView1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox4.Location = new System.Drawing.Point(227, 8);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.groupBox4.Size = new System.Drawing.Size(392, 125);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Connection Status";
            // 
            // btnStatusReset
            // 
            this.btnStatusReset.Location = new System.Drawing.Point(321, 85);
            this.btnStatusReset.Name = "btnStatusReset";
            this.btnStatusReset.Size = new System.Drawing.Size(66, 27);
            this.btnStatusReset.TabIndex = 15;
            this.btnStatusReset.Text = "RESET";
            this.btnStatusReset.UseVisualStyleBackColor = true;
            this.btnStatusReset.Click += new System.EventHandler(this.btnStatusReset_Click);
            // 
            // btnGetCommunication
            // 
            this.btnGetCommunication.Location = new System.Drawing.Point(321, 52);
            this.btnGetCommunication.Name = "btnGetCommunication";
            this.btnGetCommunication.Size = new System.Drawing.Size(66, 27);
            this.btnGetCommunication.TabIndex = 6;
            this.btnGetCommunication.Text = "GET";
            this.btnGetCommunication.UseVisualStyleBackColor = true;
            this.btnGetCommunication.Click += new System.EventHandler(this.btnGetCommunication_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Location = new System.Drawing.Point(12, 14);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.panel1.Size = new System.Drawing.Size(628, 143);
            this.panel1.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Location = new System.Drawing.Point(12, 169);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.panel2.Size = new System.Drawing.Size(628, 338);
            this.panel2.TabIndex = 17;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnTagTableSet);
            this.groupBox5.Controls.Add(this.listViewOfVariableNames);
            this.groupBox5.Location = new System.Drawing.Point(231, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(390, 317);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tag Table Variables";
            // 
            // btnTagTableSet
            // 
            this.btnTagTableSet.Location = new System.Drawing.Point(254, 279);
            this.btnTagTableSet.Name = "btnTagTableSet";
            this.btnTagTableSet.Size = new System.Drawing.Size(130, 27);
            this.btnTagTableSet.TabIndex = 13;
            this.btnTagTableSet.Text = "Tag Table Setting";
            this.btnTagTableSet.UseVisualStyleBackColor = true;
            this.btnTagTableSet.Click += new System.EventHandler(this.btnTagTableSet_Click);
            // 
            // listViewOfVariableNames
            // 
            this.listViewOfVariableNames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewOfVariableNames.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.variableNameOfTagTable,
            this.variableTypeOfTagTable,
            this.variableValueOfTagTable});
            this.listViewOfVariableNames.FullRowSelect = true;
            this.listViewOfVariableNames.GridLines = true;
            this.listViewOfVariableNames.HideSelection = false;
            this.listViewOfVariableNames.Location = new System.Drawing.Point(6, 21);
            this.listViewOfVariableNames.Name = "listViewOfVariableNames";
            this.listViewOfVariableNames.Size = new System.Drawing.Size(378, 250);
            this.listViewOfVariableNames.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewOfVariableNames.TabIndex = 1;
            this.listViewOfVariableNames.UseCompatibleStateImageBehavior = false;
            this.listViewOfVariableNames.View = System.Windows.Forms.View.Details;
            this.listViewOfVariableNames.SelectedIndexChanged += new System.EventHandler(this.listViewOfVariableNames_SelectedIndexChanged);
            // 
            // variableNameOfTagTable
            // 
            this.variableNameOfTagTable.Text = "Name";
            this.variableNameOfTagTable.Width = 110;
            // 
            // variableTypeOfTagTable
            // 
            this.variableTypeOfTagTable.Text = "Type";
            this.variableTypeOfTagTable.Width = 104;
            // 
            // variableValueOfTagTable
            // 
            this.variableValueOfTagTable.Text = "Value";
            this.variableValueOfTagTable.Width = 155;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 519);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OMRON PLC TESTER";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnReadVariableMultiple;
        private System.Windows.Forms.Button btnWriteVariable;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Button btnReadVariable;
        private System.Windows.Forms.TextBox txtVariableName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button txtReadRawMultiple;
        private System.Windows.Forms.Button btnWriteRaw;
        private System.Windows.Forms.TextBox txtBinaryValue;
        private System.Windows.Forms.Button btnReadRaw;
        private System.Windows.Forms.TextBox txtBinaryVariableName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnGetCommunication;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnTagTableSet;
        private System.Windows.Forms.ListView listViewOfVariableNames;
        private System.Windows.Forms.ColumnHeader variableNameOfTagTable;
        private System.Windows.Forms.ColumnHeader variableTypeOfTagTable;
        private System.Windows.Forms.ColumnHeader variableValueOfTagTable;
        private System.Windows.Forms.Button btnStatusReset;
    }
}

