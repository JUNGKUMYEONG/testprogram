﻿using OMRON.Compolet.CIP;
using System;

namespace OmronPLCcs
{
    public enum VariableType
    {
        TIMER = 1,
        COUNTER = 2,
        CHANNEL = 3,
        UINT_BCD = 4,
        UDINT_BCD = 5,
        ULINT_BCD = 6,
        ENUM = 7,
        DATE_NSEC = 8,
        TIME_NSEC = 9,
        DATE_AND_TIME_NSEC = 10,
        TIME_OF_DAY_NSEC = 11,
        UNION = 12,
        ASTRUCT = 160,
        STRUCT = 162,
        ARRAY = 163,
        BOOL = 193,
        SINT = 194,
        INT = 195,
        DINT = 196,
        LINT = 197,
        USINT = 198,
        UINT = 199,
        UDINT = 200,
        ULINT = 201,
        REAL = 202,
        LREAL = 203,
        STRING = 208,
        BYTE = 209,
        WORD = 210,
        DWORD = 211,
        LWORD = 212
    }
    
    public class OmronPLC
    {
        public static CommonCompolet plc = new CommonCompolet();

        public static bool Connect(string ip, int port)
        {
            try 
            {
                plc.PeerAddress = ip;
                plc.LocalPort = port;

                plc.Active = true;

                if (!plc.IsConnected)
                {
                    //Connection Status Setting
                    Config.DevInfo.IPAddress = "";
                    Config.DevInfo.nPortNum = 0;
                    Config.DevInfo.bActive = false;
                    Config.DevInfo.bIsConnected = false;
                    Config.DevInfo.sConnectionType = plc.ConnectionType;
                    Config.DevInfo.bUseRoutePath = false;
                    Config.DevInfo.sTypeName = "";

                    plc.Active = false;
                }

                plc.Active = true;

                //Connection Status Setting
                Config.DevInfo.IPAddress = plc.PeerAddress;
                Config.DevInfo.nPortNum = plc.LocalPort;
                Config.DevInfo.bActive = plc.Active;
                Config.DevInfo.bIsConnected = plc.IsConnected;
                Config.DevInfo.sConnectionType = plc.ConnectionType;
                Config.DevInfo.bUseRoutePath = plc.UseRoutePath;
                Config.DevInfo.sTypeName = plc.TypeName;

                return plc.IsConnected;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Disconnection()
        {
            try
            {
                plc.Active = false;
                //Connection Status Setting
                Config.DevInfo.IPAddress = "";
                Config.DevInfo.nPortNum = 0;
                Config.DevInfo.bActive = false;
                Config.DevInfo.bIsConnected = false;
                Config.DevInfo.sConnectionType = plc.ConnectionType;
                Config.DevInfo.bUseRoutePath = false;
                Config.DevInfo.sTypeName = "";
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}
