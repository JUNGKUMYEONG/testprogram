﻿using System;
using OMRON.Compolet.CIP;

namespace OmronPLCcs
{
    public static class Config
    {
        static xCfg _Cfg = new xCfg();

        public static xCfg.xDevInfo DevInfo => _Cfg._DevInfo;

        [Serializable]
        public class xCfg
        {
            public xDevInfo _DevInfo = new xDevInfo();

            [Serializable]
            public class xDevInfo
            {
                public int nPortNum = 2;
                public string IPAddress = "192.168.250.1";
                public bool bActive = false;
                public bool bIsConnected = false;
                public bool bUseRoutePath = false;
                public string sTypeName = "";
                public ConnectionType sConnectionType = ConnectionType.UCMM;
            }
        }
    }
}
