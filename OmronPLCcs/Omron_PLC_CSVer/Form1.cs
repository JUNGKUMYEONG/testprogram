﻿using System;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using OMRON.Compolet.CIP;

namespace OmronPLCcs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            tbIP.Text = Config.DevInfo.IPAddress;
            tbPort.Text = Config.DevInfo.nPortNum.ToString();

            btnConnect.Enabled = true;
            btnDisconnect.Enabled = false;
            ConnectionStatusSet();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            string ipAddr;
            int portNo;

            try
            {
                ipAddr = tbIP.Text;
                portNo = Convert.ToInt32(tbPort.Text);
                if(OmronPLC.Connect(ipAddr, portNo))
                {
                    btnConnect.Enabled = false;
                    btnDisconnect.Enabled = true;
                    ConnectionStatusSet();
                } else
                {
                    MessageBox.Show("Connection Failed!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                OmronPLC.Disconnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }

            btnConnect.Enabled = true;
            btnDisconnect.Enabled = false;
            ConnectionStatusSet();
        }

        private void btnGetCommunication_Click(object sender, EventArgs e)
        {
            ConnectionStatusSet();
        }

        public void ConnectionStatusSet()
        {
            listView1.Items[0].SubItems[1].Text = OmronPLC.plc.Active.ToString();
            listView1.Items[1].SubItems[1].Text = OmronPLC.plc.IsConnected.ToString();
            listView1.Items[2].SubItems[1].Text = OmronPLC.plc.TypeName.ToString();
            listView1.Items[3].SubItems[1].Text = OmronPLC.plc.ConnectionType.ToString();
            listView1.Items[4].SubItems[1].Text = OmronPLC.plc.UseRoutePath.ToString();
        }

        private void btnStatusReset_Click(object sender, EventArgs e)
        {
            listView1.Items[0].SubItems[1].Text = "";
            listView1.Items[1].SubItems[1].Text = "";
            listView1.Items[2].SubItems[1].Text = "";
            listView1.Items[3].SubItems[1].Text = "";
            listView1.Items[4].SubItems[1].Text = "";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            OmronPLC.Disconnection();
        }

        #region TagTable Variables Event Functions

        private void listViewOfVariableNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewOfVariableNames.SelectedItems.Count == 0)
            {
                return;
            }
            if (listViewOfVariableNames.SelectedItems.Count == 1)
            {
                txtVariableName.Text = listViewOfVariableNames.SelectedItems[0].Text;
                txtBinaryVariableName.Text = listViewOfVariableNames.SelectedItems[0].Text;
            }
            else
            {
                string[] varlist = new string[listViewOfVariableNames.SelectedItems.Count];
                string variables = string.Empty;
                for (int i = 0; i < listViewOfVariableNames.SelectedItems.Count; i++)
                {
                    varlist[i] = listViewOfVariableNames.SelectedItems[i].Text;
                    variables += varlist[i].ToString() + ",";
                }
                variables = variables.TrimEnd(',');
                txtVariableName.Text = variables;
                txtBinaryVariableName.Text = variables;
            }
        }

        private VariableInfo[] tags;
        private void btnTagTableSet_Click(object sender, EventArgs e)
        {
            try
            {
                /*
				 * The TagDatabaseEditor class is an user interface class for editing tags.
				 * Use the TagDatabaseEditor.dll contained in the Sysmac Gateway installation folder 
				 * (C:\Program Files\OMRON\SYSMAC Gateway\bin).
				 * [Example]`
                 * 1. Call the ShowDialog method.
				 * 2. Check the return value of the ShowDialog method.
				 * 3. Get the array of VariableInfo class by the GetVariables method or the Variables Property.
				 */
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\OMRON\SYSMAC Gateway\bin\TagDatabaseEditor.dll";
                Assembly asm = Assembly.LoadFrom(path);
                Form editor = asm.CreateInstance("OMRON.CIP.TagDatabase.TagDatabaseEditor") as Form;
                editor.Font = new Font("Consolas", 9);
                editor.Size = new Size(520, 400);
                MethodInfo mi = editor.GetType().GetMethod("SetVariables");
                object[] prms = new object[] { tags };
                mi.Invoke(editor, prms);
                if (editor.ShowDialog() == DialogResult.OK)
                {
                    mi = editor.GetType().GetMethod("GetVariables");
                    tags = mi.Invoke(editor, null) as VariableInfo[];

                    listViewOfVariableNames.Items.Clear();
                    if (tags == null)
                    {
                        return;
                    }
                    for (int i = 0; i < tags.Length; i++)
                    {
                        DisplayCipCommonVariableInfomation(tags[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnReadVariable_Click(object sender, EventArgs e)
        {
            try
            {
                string varname = txtVariableName.Text;
                object obj = OmronPLC.plc.ReadVariable(varname);
                if (obj == null)
                {
                    throw new NotSupportedException();
                }

                VariableInfo info = OmronPLC.plc.GetVariableInfo(varname);
                string str = GetValueOfVariables(obj);
                if (listViewOfVariableNames.SelectedItems.Count > 0)
                {
                    if (listViewOfVariableNames.SelectedItems[0].SubItems[0].Text == varname)
                    {
                        listViewOfVariableNames.SelectedItems[0].SubItems.Add(string.Empty);
                        listViewOfVariableNames.SelectedItems[0].SubItems[2].Text = str;
                    }
                }
                txtValue.Text = str;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private void btnWriteVariable_Click(object sender, EventArgs e)
        {
            try
            {
                // write
                object val = RemoveBrackets(txtValue.Text);
                if (OmronPLC.plc.GetVariableInfo(txtVariableName.Text).Type.Equals(VariableType.STRUCT))
                {
                    val = ObjectToByteArray(val);
                }
                OmronPLC.plc.WriteVariable(txtVariableName.Text, val);

                // read
                btnReadVariable_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnReadVariableMultiple_Click(object sender, EventArgs e)
        {
            try
            {
                string[] varlist = txtVariableName.Text.Replace(" ", String.Empty).Split(',');

                Hashtable retVals = OmronPLC.plc.ReadVariableMultiple(varlist);
                if (retVals == null)
                {
                    throw new NotSupportedException();
                }

                string multival = string.Empty;
                for (int index = 0; index < varlist.Length; index++)
                {
                    string varName = varlist[index];
                    object val = retVals[varName];
                    string valStr = GetValueOfVariables(val);
                    if (listViewOfVariableNames.SelectedItems.Count > index)
                    {
                        if (listViewOfVariableNames.SelectedItems[index].SubItems[0].Text == varlist[index])
                        {
                            listViewOfVariableNames.SelectedItems[index].SubItems.Add(string.Empty);
                            listViewOfVariableNames.SelectedItems[index].SubItems[2].Text = valStr;
                        }
                    }
                    multival += valStr + ",";
                }
                multival = multival.TrimEnd(',');
                txtValue.Text = multival;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnReadRaw_Click(object sender, EventArgs e)
        {
            try
            {
                string varname = txtBinaryVariableName.Text;
                object obj = OmronPLC.plc.ReadRawData(varname);
                VariableInfo info = OmronPLC.plc.GetVariableInfo(varname);
                string val = ByteArrayToString(obj as byte[]);

                if (listViewOfVariableNames.SelectedItems.Count > 0)
                {
                    if (listViewOfVariableNames.SelectedItems[0].SubItems[0].Text == varname)
                    {
                        listViewOfVariableNames.SelectedItems[0].SubItems.Add(string.Empty);
                        listViewOfVariableNames.SelectedItems[0].SubItems[2].Text = val;
                    }
                }
                txtBinaryValue.Text = val;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnWriteRaw_Click(object sender, EventArgs e)
        {
            try
            {
                // write
                byte[] val = StringToByteArray(txtBinaryValue.Text.Replace("-", string.Empty));
                OmronPLC.plc.WriteRawData(txtBinaryVariableName.Text, val);

                // read
                btnReadRaw_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtReadRawMultiple_Click(object sender, EventArgs e)
        {
            try
            {
                string[] varlist = txtBinaryVariableName.Text.Replace(" ", String.Empty).Split(',');

                Hashtable retVals = OmronPLC.plc.ReadRawDataMultiple(varlist);
                string multival = string.Empty;
                for (int index = 0; index < varlist.Length; index++)
                {
                    string varName = varlist[index];
                    string val = ByteArrayToString(retVals[varName] as byte[]);

                    if (listViewOfVariableNames.SelectedItems.Count > index)
                    {
                        if (listViewOfVariableNames.SelectedItems[index].SubItems[0].Text == varlist[index])
                        {
                            listViewOfVariableNames.SelectedItems[index].SubItems.Add(string.Empty);
                            listViewOfVariableNames.SelectedItems[index].SubItems[2].Text = val;
                        }
                    }
                    multival += val + ",";
                }
                multival = multival.TrimEnd(',');
                txtBinaryValue.Text = multival;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion


        #region Helper Functions

        private void DisplayCipCommonVariableInfomation(VariableInfo val)
        {
            ListViewItem item = listViewOfVariableNames.Items.Add(val.Name);
            if (val.IsArray)
            {
                string text = val.Type.ToString();
                foreach (long num in val.NumberOfElements)
                {
                    text += "[" + num.ToString() + "]";
                }
                item.SubItems.Add(text);
            }
            else
            {
                item.SubItems.Add(val.Type.ToString());
            }
            item.SubItems.Add(string.Empty);
        }

        private string GetValueOfVariables(object val)
        {
            string valStr = string.Empty;
            if (val.GetType().IsArray)
            {
                Array valArray = val as Array;
                if (valArray.Rank == 1)
                {
                    valStr += "[";
                    foreach (object a in valArray)
                    {
                        valStr += GetValueString(a) + ",";
                    }
                    valStr = valStr.TrimEnd(',');
                    valStr += "]";
                }
                else if (valArray.Rank == 2)
                {
                    for (int i = 0; i <= valArray.GetUpperBound(0); i++)
                    {
                        valStr += "[";
                        for (int j = 0; j <= valArray.GetUpperBound(1); j++)
                        {
                            valStr += GetValueString(valArray.GetValue(i, j)) + ",";
                        }
                        valStr = valStr.TrimEnd(',');
                        valStr += "]";
                    }
                }
                else if (valArray.Rank == 3)
                {
                    for (int i = 0; i <= valArray.GetUpperBound(0); i++)
                    {
                        for (int j = 0; j <= valArray.GetUpperBound(1); j++)
                        {
                            valStr += "[";
                            for (int z = 0; z <= valArray.GetUpperBound(2); z++)
                            {
                                valStr += GetValueString(valArray.GetValue(i, j, z)) + ",";
                            }
                            valStr = valStr.TrimEnd(',');
                            valStr += "]";
                        }
                    }
                }
            }
            else
            {
                valStr = GetValueString(val);
            }
            return valStr;
        }

        private string GetValueString(object val)
        {
            //data type check
            if (val is float || val is double)
            {
                return string.Format("{0:R}", val);
            }
            else
            {
                return val.ToString();
            }
        }

        private object RemoveBrackets(string val)
        {
            object obj = string.Empty;
            if (val.IndexOf("[") >= 0)
            {
                string str = val.Trim('[', ']');
                str = str.Replace("][", ",");
                obj = str.Split(',');
            }
            else
            {
                obj = val;
            }
            return obj;
        }

        private string ByteArrayToString(byte[] ba)
        {
            if (ba.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return BitConverter.ToString(ba);
            }
        }

        private byte[] StringToByteArray(string hex)
        {
            if (hex == String.Empty)
            {
                return new Byte[0];
            }
            int byteNumber = hex.Length / 2;
            byte[] bytes = new byte[byteNumber];
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }

        private byte[] ObjectToByteArray(object obj)
        {
            if (obj is Array)
            {
                Array arr = obj as Array;
                Byte[] bin = new Byte[arr.Length];
                for (int i = 0; i < bin.Length; i++)
                {
                    bin[i] = Convert.ToByte(arr.GetValue(i));
                }
                return bin;
            }
            else
            {
                return new Byte[1] { Convert.ToByte(obj) };
            }
        }

        #endregion
        
    }
}
